
using UnityEngine;
using PathologicalGames;

public class FruitSpawn : MonoBehaviour
{
     private void Start()
    {
        var fruit = PoolManager.Pools["Game"].Spawn("Melon", transform.position,Quaternion.identity, transform.parent);
        fruit.position = transform.position;
    }
}
