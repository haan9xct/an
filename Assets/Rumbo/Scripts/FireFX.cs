﻿
using UnityEngine;
using DG.Tweening;

public class FireFX : MonoBehaviour
{
    void Start()
    {
        transform.localScale = new Vector3(2, 2, 2);
        transform.DOScale(new Vector3(5, 5, 5), 0.8f);
    }
    private void OnDestroy()
    {
        transform.DOScale(new Vector3(1, 1, 1), 0.5f);
    }

}
