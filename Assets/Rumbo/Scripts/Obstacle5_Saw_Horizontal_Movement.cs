﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle5_Saw_Horizontal_Movement : MonoBehaviour
{
    [Header("Movement parameters")] public bool moving;
    public bool moveFromLeft;
    public float movementRange;
    public Transform pivotRotate;
    public float timePerRound;
    public bool clockwise = true;
    Tween tween;

    private void Start()
    {
        // Static saw
        if (!moving)
        {
            return;
        }

        // Mobile saw
        if (moveFromLeft)
        {
            MoveRight();
        }
        else
        {
            MoveLeft();
        }
    }


    private void MoveRight()
    {
        RotateBody(0, 0, 360);

        transform.DOLocalMoveX(transform.localPosition.x + movementRange, 2.5f).SetEase(Ease.InOutQuad).OnKill(MoveLeft)
            .SetDelay(0.1f);
    }


    private void MoveLeft()
    {
        RotateBody(0, 0, 360);

        transform.DOLocalMoveX(transform.localPosition.x - movementRange, 2.5f).SetEase(Ease.InOutQuad)
            .OnKill(MoveRight).SetDelay(0.1f);

    }

    public void RotateBody(float axisX, float axisY, float axisZ)
    {
        Vector3 currentRotation = pivotRotate.rotation.eulerAngles;
        var angleDown = new Vector3(currentRotation.x + axisX, currentRotation.y + axisY, currentRotation.z + axisZ);
        var angleOpposite =
            new Vector3(currentRotation.x - axisX, currentRotation.y - axisY, currentRotation.z - axisZ);
        tween = pivotRotate.DORotate(clockwise ? angleDown : angleOpposite, timePerRound, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
    }
}