﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class Scaler : MonoBehaviour
{
    public SkinnedMeshRenderer meshRenderer;
    public float duration;
    bool isRun = false;
    float current;
    float a;

    float to = 100;
    void Start()
    {
        Debug.Log(meshRenderer.GetBlendShapeWeight(0));
    }
    [Button]
    public void Show()
    {
        isRun = true;
        //Debug.Log(meshRenderer.GetBlendShapeWeight(0));
        current = meshRenderer.GetBlendShapeWeight(0);
        //meshRenderer.GetBlendShapeWeight()
    }
    private void Update()
    {
        if (isRun)
        {
             float value = Mathf.Lerp(current, to, duration * Time.deltaTime);
            // meshRenderer.SetBlendShapeWeight((int)current,value);
             current = value;
             Debug.Log(value);
            //to += 10;
            if (value >= to )
            {
                isRun = false;
            }
        }

    }

}
