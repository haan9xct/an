﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle_SelfSpinner : MonoBehaviour
{
	public enum spinAxis
	{
		x,
		y,
		z
	};

	public spinAxis spinningAxis;
	public float timePerRound;
	public bool clockwise = true;


	// Start is called before the first frame update
	void Start()
	{
		Vector3 currentRotation = transform.rotation.eulerAngles;

		if (spinningAxis == spinAxis.x)
		{
			if (clockwise)
			{
				transform.DORotate(new Vector3(currentRotation.x + 360f, currentRotation.y, currentRotation.z), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
			else
			{
				transform.DORotate(new Vector3(currentRotation.x - 360f, currentRotation.y, currentRotation.z), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
		}
		if (spinningAxis == spinAxis.y)
		{
			if (clockwise)
			{
				transform.DORotate(new Vector3(currentRotation.x, currentRotation.y + 360f, currentRotation.z), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
			else
			{
				transform.DORotate(new Vector3(currentRotation.x, currentRotation.y - 360f, currentRotation.z), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
		}
		if (spinningAxis == spinAxis.z)
		{
			if (clockwise)
			{
				transform.DORotate(new Vector3(currentRotation.x, currentRotation.y, currentRotation.z + 360f), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
			else
			{
				transform.DORotate(new Vector3(currentRotation.x, currentRotation.y, currentRotation.z - 360f), timePerRound, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
			}
		}
	}
}
