﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PathCreation;

namespace PathCreationEditor
{
    public static class PathHandle
    {

        public const float extraInputRadius = .005f;

        static Vector2 handleDragMouseStart;
        static Vector2 handleDragMouseEnd;
        static Vector3 handleDragWorldStart;

        static int selectedHandleID;
        static bool mouseIsOverAHandle;

        public enum HandleInputType
        {
            None,
            LMBPress,
            LMBClick,
            LMBDrag,
            LMBRelease,
        };

        static float dstMouseToDragPointStart;

        static List<int> ids;
        static HashSet<int> idHash;

        static PathHandle()
        {
            ids = new List<int>();
            idHash = new HashSet<int>();

            dstMouseToDragPointStart = float.MaxValue;
        }
        public struct HandleColours
        {
            public Color defaultColour;
            public Color highlightedColour;
            public Color selectedColour;
            public Color disabledColour;

            public HandleColours(Color defaultColour, Color highlightedColour, Color selectedColour, Color disabledColour)
            {
                this.defaultColour = defaultColour;
                this.highlightedColour = highlightedColour;
                this.selectedColour = selectedColour;
                this.disabledColour = disabledColour;
            }
        }

        static void AddIDs(int upToIndex)
        {
            int numIDAtStart = ids.Count;
            int numToAdd = (upToIndex - numIDAtStart) + 1;
            for (int i = 0; i < numToAdd; i++)
            {
                string hashString = string.Format("pathhandle({0})", numIDAtStart + i);
                int hash = hashString.GetHashCode();

                int id = GUIUtility.GetControlID(hash, FocusType.Passive);
                int numIts = 0;

                    numIts ++;
                    id += numIts * numIts;
                   
                }

            }
        }
}