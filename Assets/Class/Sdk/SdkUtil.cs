﻿#define ENABLE_MYLOG

using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace mygame.sdk
{
    public class SdkUtil
    {
        public static int levelLog = 0;//0-debug; 1-warring; 2-error
        public static long systemCurrentMiliseconds()
        {
            var re = (DateTime.UtcNow.Ticks - 621355968000000000) / 10000;
            return re;
        }
       
        public static bool isiPad()
        {
#if UNITY_IOS || UNITY_IPHONE
            if (UnityEngine.iOS.Device.generation.ToString().Contains("iPad"))
            {
                return true;
            }
            else
            {
                return false;
            }
#else
            float w = Screen.width;
            float h = Screen.height;
            if (h < w) {
                    float th = h;
                    h = w;
                    w = th;
                }
            if (w > 0) {
                float per = h/w;
                if (per < 1.49f) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
#endif
        }

     
    }
}