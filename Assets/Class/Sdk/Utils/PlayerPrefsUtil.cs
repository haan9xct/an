﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace mygame.sdk
{
    public class PlayerPrefsUtil
    {
        public static int Level
        {
            get { return PlayerPrefsBase.Instance().getInt("play_level", 1); }
            set { PlayerPrefsBase.Instance().setInt("play_level", value); }
        }

        public static int NormalLevelBonusCount
        {
            get { return PlayerPrefsBase.Instance().getInt("normal_level_bonus_count", 0); }
            set { PlayerPrefsBase.Instance().setInt("normal_level_bonus_count", value); }
        }

        public static int memGunBoss
        {
            get { return PlayerPrefsBase.Instance().getInt("memBossGun", -1); }
            set { PlayerPrefsBase.Instance().setInt("memBossGun", value); }
        }

        public static int Count4Hard3
        {
            get { return PlayerPrefsBase.Instance().getInt("Count4Hard3", 0); }
            set { PlayerPrefsBase.Instance().setInt("Count4Hard3", value); }
        }

        public static int CountBattleItem
        {
            get { return PlayerPrefsBase.Instance().getInt("CountForHasTurnRun", 0); }
            set { PlayerPrefsBase.Instance().setInt("CountForHasTurnRun", value); }
        }

        public static int Count4Hard4
        {
            get { return PlayerPrefsBase.Instance().getInt("Count4Hard4", 0); }
            set { PlayerPrefsBase.Instance().setInt("Count4Hard4", value); }
        }

        public static int countKeyGift
        {
            get { return PlayerPrefsBase.Instance().getInt("countKeyGift", 0); }
            set { PlayerPrefsBase.Instance().setInt("countKeyGift", value); }
        }

        public static int PerGetKeyAndGift
        {
            get { return PlayerPrefsBase.Instance().getInt("PerGetKeyAndGift", 30); }
            set { PlayerPrefsBase.Instance().setInt("PerGetKeyAndGift", value); }
        }

        public static int PerGetOnlyKeyOrGift
        {
            get { return PlayerPrefsBase.Instance().getInt("PerGetOnlyKeyOrGift", 10); }
            set { PlayerPrefsBase.Instance().setInt("PerGetOnlyKeyOrGift", value); }
        }

        public static int enemyLvPower
        {
            get { return PlayerPrefsBase.Instance().getInt("enemyLvPower", 1); }
            set { PlayerPrefsBase.Instance().setInt("enemyLvPower", value); }
        }

        public static int enemyLvPowerDeltaic
        {
            get { return PlayerPrefsBase.Instance().getInt("enemyLvPowerDeltaic", 0); }
            set { PlayerPrefsBase.Instance().setInt("enemyLvPowerDeltaic", value); }
        }

        public static int isUsingSkin
        {
            get { return PlayerPrefsBase.Instance().getInt("isUsingSkin", 0); }
            set { PlayerPrefsBase.Instance().setInt("isUsingSkin", value); }
        }

        public static int currentMemBossGun
        {
            get { return PlayerPrefsBase.Instance().getInt("currentMemBossGun", 1); }
            set { PlayerPrefsBase.Instance().setInt("currentMemBossGun", value); }
        }

        public static int IsOwnedStarterPack
        {
            get { return PlayerPrefsBase.Instance().getInt("IsOwnedStarterPack", 0); }
            set { PlayerPrefsBase.Instance().setInt("IsOwnedStarterPack", value); }
        }

        public static int enemyLvArmor
        {
            get { return PlayerPrefsBase.Instance().getInt("enemyLvArmor", 1); }
            set { PlayerPrefsBase.Instance().setInt("enemyLvArmor", value); }
        }

        public static int enemyLvArmorDeltaic
        {
            get { return PlayerPrefsBase.Instance().getInt("enemyLvArmorDeltaic", 0); }
            set { PlayerPrefsBase.Instance().setInt("enemyLvArmorDeltaic", value); }
        }

        public static int IsMoveFollowATouch
        {
            get { return PlayerPrefsBase.Instance().getInt("IsMoveFollowATouch", 0); }
            set { PlayerPrefsBase.Instance().setInt("IsMoveFollowATouch", value); }
        }

        public static float maxMoveWhen1
        {
            get { return PlayerPrefsBase.Instance().getFloat("maxMoveWhen1", 0.8f); }
            set { PlayerPrefsBase.Instance().setFloat("maxMoveWhen1", value); }
        }

        public static int TypeBattleFight
        {
            get { return PlayerPrefsBase.Instance().getInt("cf_TypeBattleFight", 1); }
            set { PlayerPrefsBase.Instance().setInt("cf_TypeBattleFight", value); }
        }

        public static int IsUpInfoAlly
        {
            get { return PlayerPrefsBase.Instance().getInt("cf_IsUpInfoAlly", 1); }
            set { PlayerPrefsBase.Instance().setInt("cf_IsUpInfoAlly", value); }
        }

        public static int IsSecondPlay
        {
            get { return PlayerPrefsBase.Instance().getInt("play_second", 0); }
            set { PlayerPrefsBase.Instance().setInt("play_second", value); }
        }

        public static int FirstPlayBonusLevel
        {
            get { return PlayerPrefsBase.Instance().getInt("first_play_bonus_level", 0); }
            set { PlayerPrefsBase.Instance().setInt("first_play_bonus_level", value); }
        }

        public static int isShowBtnCloseRatePn
        {
            get { return PlayerPrefsBase.Instance().getInt("isShowBtnCloseRatePn", 1); }
            set { PlayerPrefsBase.Instance().setInt("isShowBtnCloseRatePn", value); }
        }

        public static int levelShowRating1
        {
            get { return PlayerPrefsBase.Instance().getInt("levelShowRating1", 15); }
            set { PlayerPrefsBase.Instance().setInt("levelShowRating1", value); }
        }

        public static int levelShowRating2
        {
            get { return PlayerPrefsBase.Instance().getInt("levelShowRating2", 25); }
            set { PlayerPrefsBase.Instance().setInt("levelShowRating2", value); }
        }

        public static int minVelocityHoz
        {
            get { return PlayerPrefsBase.Instance().getInt("cf_minVelocityHoz", 10); }
            set { PlayerPrefsBase.Instance().setInt("cf_minVelocityHoz", value); }
        }

        public static int maxVelocityHoz
        {
            get { return PlayerPrefsBase.Instance().getInt("cf_maxVelocityHoz", 16); }
            set { PlayerPrefsBase.Instance().setInt("cf_maxVelocityHoz", value); }
        }

        public static int sensitivity
        {
            get { return PlayerPrefsBase.Instance().getInt("cf_sensitivity", 45); }
            set { PlayerPrefsBase.Instance().setInt("cf_sensitivity", value); }
        }

        public static int memIdxbonusMap
        {
            get { return PlayerPrefsBase.Instance().getInt("play_level_bonus", -2); }
            set { PlayerPrefsBase.Instance().setInt("play_level_bonus", value); }
        }

        public static string MemMapgen
        {
            get { return PlayerPrefsBase.Instance().getString("mem_map_genlv", ""); }
            set { PlayerPrefsBase.Instance().setString("mem_map_genlv", value); }
        }

        public static int memCastleCurr
        {
            get { return PlayerPrefsBase.Instance().getInt("mem_CastleCurlv", -1); }
            set { PlayerPrefsBase.Instance().setInt("mem_CastleCurlv", value); }
        }

        public static string memdataenemy
        {
            get { return PlayerPrefsBase.Instance().getString("mem_data_enemylv", ""); }
            set { PlayerPrefsBase.Instance().setString("mem_data_enemylv", value); }
        }

        public static int ATKLevel
        {
            get { return PlayerPrefsBase.Instance().getInt("ATK_Level", 1); }
            set { PlayerPrefsBase.Instance().setInt("ATK_Level", value); }
        }

        public static int DEFLevel
        {
            get { return PlayerPrefsBase.Instance().getInt("DEF_Level", 1); }
            set { PlayerPrefsBase.Instance().setInt("DEF_Level", value); }
        }

        public static int CoinHavestLevel
        {
            get { return PlayerPrefsBase.Instance().getInt("CoinHavestLevel", 1); }
            set { PlayerPrefsBase.Instance().setInt("CoinHavestLevel", value); }
        }

        public static int CurrentCoinPlayer
        {
            get { return PlayerPrefsBase.Instance().getInt("Current_Coin_Player", 0); }
            set { PlayerPrefsBase.Instance().setInt("Current_Coin_Player", value); }
        }

        public static int CurrentKeyPlayer
        {
            get { return PlayerPrefsBase.Instance().getInt("CurrentKeyPlayer", 0); }
            set { PlayerPrefsBase.Instance().setInt("CurrentKeyPlayer", value); }
        }

        public static int CurrentChanceAttack
        {
            get { return PlayerPrefsBase.Instance().getInt("CurrentChanceAttack", 0); }
            set { PlayerPrefsBase.Instance().setInt("CurrentChanceAttack", value); }
        }

        public static string CurrentWeaponItem
        {
            get { return PlayerPrefsBase.Instance().getString("current_weapon_item", "AK47Classic"); }
            set { PlayerPrefsBase.Instance().setString("current_weapon_item", value); }
        }

        public static int ProcessGiftBonus
        {
            get { return PlayerPrefsBase.Instance().getInt("ProcessGiftBonus", 0); }
            set { PlayerPrefsBase.Instance().setInt("ProcessGiftBonus", value); }
        }

        public static string ItemOwned
        {
            get { return PlayerPrefsBase.Instance().getString("ItemOwned", ""); }
            set { PlayerPrefsBase.Instance().setString("ItemOwned", value); }
        }

        public static string WeaponOwned
        {
            get { return PlayerPrefsBase.Instance().getString("WeaponOwned", ""); }
            set { PlayerPrefsBase.Instance().setString("WeaponOwned", value); }
        }

        public static string CurrentItemOutfit
        {
            get { return PlayerPrefsBase.Instance().getString("CurrentItemOutfit", ""); }
            set { PlayerPrefsBase.Instance().setString("CurrentItemOutfit", value); }
        }

        public static string CurrentBonusGift
        {
            get { return PlayerPrefsBase.Instance().getString("CurrentBonusGift", "head1"); }
            set { PlayerPrefsBase.Instance().setString("CurrentBonusGift", value); }
        }

        public static int MusicSetting
        {
            get { return PlayerPrefsBase.Instance().getInt("MusicSetting", 0); }
            set { PlayerPrefsBase.Instance().setInt("MusicSetting", value); }
        }

        public static int isShowMecommentRemoveAds
        {
            get { return PlayerPrefsBase.Instance().getInt("is_recom_rmads", 0); }
            set { PlayerPrefsBase.Instance().setInt("is_recom_rmads", value); }
        }

        public static int isShowRate
        {
            get { return PlayerPrefsBase.Instance().getInt("is_show_rate", 0); }
            set { PlayerPrefsBase.Instance().setInt("is_show_rate", value); }
        }
        public static int CurrentDayReward
        {
            get { return PlayerPrefsBase.Instance().getInt("current_day_reward", 0); }
            set { PlayerPrefsBase.Instance().setInt("current_day_reward", value); }
        }
        public static string NextTimeToGetGift
        {
            get { return PlayerPrefsBase.Instance().getString("next_time_reward", ""); }
            set { PlayerPrefsBase.Instance().setString("next_time_reward", value); }
        }
        public static int StatusDailyGift
        {
            get { return PlayerPrefsBase.Instance().getInt("status_daily_gift", 1); }
            set { PlayerPrefsBase.Instance().setInt("status_daily_gift", value); }
        }
        public static string InventoryData
        {
            get { return PlayerPrefsBase.Instance().getString("inventory_data", ""); }
            set { PlayerPrefsBase.Instance().setString("inventory_data", value); }
        }
    }
}