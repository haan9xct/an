
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;


namespace MyJson
{
    internal class Lexer
    {
        public enum Token
        {
            None,
            Null,
            True,
            False,
            Colon,
            Comma,
            String,
            Number,
            CurlyOpen,
            CurlyClose,
            SquaredOpen,
            SquaredClose
        }

        private int index;

        private readonly char[] json;
        private readonly char[] stringBuffer = new char[4096];
        private bool success = true;

        public Lexer(string text)
        {
            Reset();

            json = text.ToCharArray();
            parseNumbersAsFloat = false;
        }

        public bool hasError => !success;

        public int lineNumber { get; private set; }

        public bool parseNumbersAsFloat { get; set; }

        public void Reset()
        {
            index = 0;
            lineNumber = 1;
            success = true;
        }

        public string ParseString()
        {
            var idx = 0;
            StringBuilder builder = null;

            SkipWhiteSpaces();

            // "
            var c = json[index++];

            var failed = false;
            var complete = false;

            while (!complete && !failed)
            {
                if (index == json.Length)
                    break;

                c = json[index++];
                if (c == '"')
                {
                    complete = true;
                    break;
                }

                if (c == '\\')
                {
                    if (index == json.Length)
                        break;

                    c = json[index++];

                    switch (c)
                    {
                        case '"':
                            stringBuffer[idx++] = '"';
                            break;
                        case '\\':
                            stringBuffer[idx++] = '\\';
                            break;
                        case '/':
                            stringBuffer[idx++] = '/';
                            break;
                        case 'b':
                            stringBuffer[idx++] = '\b';
                            break;
                        case 'f':
                            stringBuffer[idx++] = '\f';
                            break;
                        case 'n':
                            stringBuffer[idx++] = '\n';
                            break;
                        case 'r':
                            stringBuffer[idx++] = '\r';
                            break;
                        case 't':
                            stringBuffer[idx++] = '\t';
                            break;
                        case 'u':
                            var remainingLength = json.Length - index;
                            if (remainingLength >= 4)
                            {
                                var hex = new string(json, index, 4);

                                // XXX: handle UTF
                                stringBuffer[idx++] = (char) Convert.ToInt32(hex, 16);

                                // skip 4 chars
                                index += 4;
                            }
                            else
                            {
                                failed = true;
                            }

                            break;
                    }
                }
                else
                {
                    stringBuffer[idx++] = c;
                }

                if (idx >= stringBuffer.Length)
                {
                    if (builder == null)
                        builder = new StringBuilder();

                    // builder.Append(stringBuffer, 0, idx);
                    idx = 0;
                }
            }

            if (!complete)
            {
                success = false;
                return null;
            }

            if (builder != null)
                return builder.ToString();
            return new string(stringBuffer, 0, idx);
        }

        private string GetNumberString()
        {
            SkipWhiteSpaces();

            var lastIndex = GetLastIndexOfNumber(index);
            var charLength = lastIndex - index + 1;

            var result = new string(json, index, charLength);

            index = lastIndex + 1;

            return result;
        }

       

        private int GetLastIndexOfNumber(int index)
        {
            int lastIndex;

            for (lastIndex = index; lastIndex < json.Length; lastIndex++)
            {
                var ch = json[lastIndex];

                if ((ch < '0' || ch > '9') && ch != '+' && ch != '-'
                    && ch != '.' && ch != 'e' && ch != 'E')
                    break;
            }

            return lastIndex - 1;
        }

        private void SkipWhiteSpaces()
        {
            for (; index < json.Length; index++)
            {
                var ch = json[index];

                if (ch == '\n')
                    lineNumber++;

                if (!char.IsWhiteSpace(json[index]))
                    break;
            }
        }

        public Token LookAhead()
        {
            SkipWhiteSpaces();

            var savedIndex = index;
            return NextToken(json, ref savedIndex);
        }

        public Token NextToken()
        {
            SkipWhiteSpaces();
            return NextToken(json, ref index);
        }

        private static Token NextToken(char[] json, ref int index)
        {
            if (index == json.Length)
                return Token.None;

            var c = json[index++];

            switch (c)
            {
                case '{':
                    return Token.CurlyOpen;
                case '}':
                    return Token.CurlyClose;
                case '[':
                    return Token.SquaredOpen;
                case ']':
                    return Token.SquaredClose;
                case ',':
                    return Token.Comma;
                case '"':
                    return Token.String;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                    return Token.Number;
                case ':':
                    return Token.Colon;
            }

            index--;

            var remainingLength = json.Length - index;

            // false
            if (remainingLength >= 5)
                if (json[index] == 'f' &&
                    json[index + 1] == 'a' &&
                    json[index + 2] == 'l' &&
                    json[index + 3] == 's' &&
                    json[index + 4] == 'e')
                {
                    index += 5;
                    return Token.False;
                }

            // true
            if (remainingLength >= 4)
                if (json[index] == 't' &&
                    json[index + 1] == 'r' &&
                    json[index + 2] == 'u' &&
                    json[index + 3] == 'e')
                {
                    index += 4;
                    return Token.True;
                }

            // null
            if (remainingLength >= 4)
                if (json[index] == 'n' &&
                    json[index + 1] == 'u' &&
                    json[index + 2] == 'l' &&
                    json[index + 3] == 'l')
                {
                    index += 4;
                    return Token.Null;
                }

            return Token.None;
        }
    }

    public class JsonDecoder
    {
        private Lexer lexer;

        public JsonDecoder()
        {
            errorMessage = null;
            parseNumbersAsFloat = false;
        }

        public string errorMessage { get; private set; }

        public bool parseNumbersAsFloat { get; set; }

        public object Decode(string text)
        {
            errorMessage = null;

            lexer = new Lexer(text);
            lexer.parseNumbersAsFloat = parseNumbersAsFloat;

            return ParseValue();
        }

        public static object DecodeText(string text)
        {
            var builder = new JsonDecoder();
            return builder.Decode(text);
        }

        private IDictionary<string, object> ParseObject()
        {
            var table = new Dictionary<string, object>();

            // {
            lexer.NextToken();

            while (true)
            {
                var token = lexer.LookAhead();

                switch (token)
                {
                    case Lexer.Token.None:
                        TriggerError("Invalid token");
                        return null;
                    case Lexer.Token.Comma:
                        lexer.NextToken();
                        break;
                    case Lexer.Token.CurlyClose:
                        lexer.NextToken();
                        return table;
                    default:
                        // name
                        var name = EvalLexer(lexer.ParseString());

                        if (errorMessage != null)
                            return null;

                        // :
                        token = lexer.NextToken();

                        if (token != Lexer.Token.Colon)
                        {
                            TriggerError("Invalid token; expected ':'");
                            return null;
                        }

                        // value
                        var value = ParseValue();

                        if (errorMessage != null)
                            return null;

                        table[name] = value;
                        break;
                }
            }

            //return null; // Unreachable code
        }

        private IList<object> ParseArray()
        {
            var array = new List<object>();

            // [
            lexer.NextToken();

            while (true)
            {
                var token = lexer.LookAhead();

                switch (token)
                {
                    case Lexer.Token.None:
                        TriggerError("Invalid token");
                        return null;
                    case Lexer.Token.Comma:
                        lexer.NextToken();
                        break;
                    case Lexer.Token.SquaredClose:
                        lexer.NextToken();
                        return array;
                    default:
                        var value = ParseValue();

                        if (errorMessage != null)
                            return null;

                        array.Add(value);
                        break;
                }
            }

            //return null; // Unreachable code
        }

        private object ParseValue()
        {
            switch (lexer.LookAhead())
            {
                case Lexer.Token.String:
                    return EvalLexer(lexer.ParseString());
              
                case Lexer.Token.CurlyOpen:
                    return ParseObject();
                case Lexer.Token.SquaredOpen:
                    return ParseArray();
                case Lexer.Token.True:
                    lexer.NextToken();
                    return true;
                case Lexer.Token.False:
                    lexer.NextToken();
                    return false;
                case Lexer.Token.Null:
                    lexer.NextToken();
                    return null;
                case Lexer.Token.None:
                    break;
            }

            TriggerError("Unable to parse value");
            return null;
        }

        private void TriggerError(string message)
        {
            errorMessage = string.Format("Error: '{0}' at line {1}",
                message, lexer.lineNumber);
        }

        private T EvalLexer<T>(T value)
        {
            if (lexer.hasError)
                TriggerError("Lexical error ocurred");

            return value;
        }
    }
}