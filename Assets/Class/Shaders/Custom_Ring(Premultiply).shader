Shader "Custom/Ring(Premultiply)"
{
  Properties
  {
    _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
    _MainTex ("Particle Texture", 2D) = "white" {}
    _InvFade ("Soft Particles Factor", Range(0.01, 3)) = 1
    _AlphaBlend ("AlphaBlend", Range(0, 1)) = 0.6
    [Toggle] _FlipDir ("FlipDir", float) = 0
    _Color ("Color", Color) = (0,0,0,0)
  }
  SubShader
  {
    Tags
    { 
      "IGNOREPROJECTOR" = "true"
      "PreviewType" = "Plane"
      "QUEUE" = "Transparent"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "PreviewType" = "Plane"
        "QUEUE" = "Transparent"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Blend One OneMinusSrcAlpha
      ColorMask RGB
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _TintColor;
      uniform float4 _MainTex_ST;
      uniform float _FlipDir;
      uniform float4 _Color;
      uniform float _AlphaBlend;
      uniform sampler2D _MainTex;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.color = in_v.color;
          out_v.texcoord = in_v.texcoord;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float4 u_xlat0_d;
      float u_xlat16_0;
      float4 u_xlat10_0;
      float4 u_xlat1_d;
      float3 u_xlat3;
      float u_xlat7;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat16_0 = (float(1) / in_f.color.x);
          u_xlat1_d.yz = TRANSFORM_TEX(in_f.texcoord.xy, _MainTex);
          u_xlat7 = ((-u_xlat1_d.y) + 1);
          u_xlat7 = (((-u_xlat7) * u_xlat16_0) + 1);
          u_xlat3.x = ((u_xlat1_d.y * u_xlat16_0) + (-u_xlat7));
          u_xlat1_d.x = ((_FlipDir * u_xlat3.x) + u_xlat7);
          u_xlat1_d.y = ((-u_xlat1_d.x) + 1);
          u_xlat3.xz = ceil(u_xlat1_d.yx);
          u_xlat10_0 = tex2D(_MainTex, u_xlat1_d.xz);
          u_xlat1_d.x = (u_xlat3.z * u_xlat3.x);
          u_xlat1_d.x = clamp(u_xlat1_d.x, 0, 1);
          u_xlat1_d.x = (u_xlat10_0.w * u_xlat1_d.x);
          u_xlat1_d.yzw = (u_xlat10_0.xyz * _TintColor.xyz);
          u_xlat1_d.x = (u_xlat1_d.x * in_f.color.w);
          u_xlat1_d.x = (u_xlat1_d.x * _TintColor.w);
          u_xlat1_d = (u_xlat1_d * _Color.wxyz);
          u_xlat0_d.xyz = float3((u_xlat1_d.xxx * u_xlat1_d.yzw));
          u_xlat0_d.w = (u_xlat1_d.x * _AlphaBlend);
          out_f.color = u_xlat0_d;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
