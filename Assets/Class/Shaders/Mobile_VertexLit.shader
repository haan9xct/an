// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

Shader "Mobile/VertexLit"
{
  Properties
  {
    _MainTex ("Base (RGB)", 2D) = "white" {}
  }
  SubShader
  {
    Tags
    { 
      "RenderType" = "Opaque"
    }
    LOD 80
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "LIGHTMODE" = "Vertex"
        "RenderType" = "Opaque"
      }
      LOD 80
      Fog
      { 
        Mode  Off
      } 
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      #define conv_mxt4x4_3(mat4x4) float4(mat4x4[0].w,mat4x4[1].w,mat4x4[2].w,mat4x4[3].w)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4 unity_LightColor[8];
      //uniform float4 unity_LightPosition[8];
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4 glstate_lightmodel_ambient;
      //uniform float4x4 unity_MatrixInvV;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _MainTex_ST;
      uniform sampler2D _MainTex;
      struct appdata_t
      {
          float3 vertex :POSITION0;
          float3 normal :NORMAL0;
          float3 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float4 color :COLOR0;
          float2 texcoord :TEXCOORD0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 color :COLOR0;
          float2 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      float3 u_xlat16_2;
      float3 u_xlat16_3;
      float3 u_xlat16_4;
      float u_xlat15;
      float u_xlat16_17;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat0.xyz = (conv_mxt4x4_1(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).yyy).xyz;
          u_xlat0.xyz = float3(((conv_mxt4x4_0(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).xxx) + u_xlat0.xyz));
          u_xlat0.xyz = float3(((conv_mxt4x4_2(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).zzz) + u_xlat0.xyz));
          u_xlat0.xyz = float3(((conv_mxt4x4_3(unity_WorldToObject).xyz * conv_mxt4x4_0(unity_MatrixInvV).www) + u_xlat0.xyz));
          u_xlat0.x = dot(u_xlat0.xyz, in_v.normal.xyz);
          u_xlat1.xyz = mul(unity_MatrixInvV, unity_WorldToObject[0]);
          u_xlat0.y = dot(u_xlat1.xyz, in_v.normal.xyz);
          u_xlat1.xyz = (conv_mxt4x4_1(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).yyy).xyz;
          u_xlat1.xyz = float3(((conv_mxt4x4_0(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).xxx) + u_xlat1.xyz));
          u_xlat1.xyz = float3(((conv_mxt4x4_2(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).zzz) + u_xlat1.xyz));
          u_xlat1.xyz = float3(((conv_mxt4x4_3(unity_WorldToObject).xyz * conv_mxt4x4_2(unity_MatrixInvV).www) + u_xlat1.xyz));
          u_xlat0.z = dot(u_xlat1.xyz, in_v.normal.xyz);
          u_xlat0.xyz = float3(normalize(u_xlat0.xyz));
          u_xlat16_2.x = dot(u_xlat0.xyz, unity_LightPosition[0].xyz);
          u_xlat16_2.x = max(u_xlat16_2.x, 0);
          u_xlat16_2.xyz = float3((u_xlat16_2.xxx * unity_LightColor[0].xyz));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_2.xyz = float3(min(u_xlat16_2.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + glstate_lightmodel_ambient.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[1].xyz);
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_3.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[1].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_3.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[2].xyz);
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_3.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[2].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_3.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[3].xyz);
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_3.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[3].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_3.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[4].xyz);
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_3.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[4].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_3.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[5].xyz);
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_3.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[5].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_3.xyz));
          u_xlat16_17 = dot(u_xlat0.xyz, unity_LightPosition[6].xyz);
          u_xlat16_3.x = dot(u_xlat0.xyz, unity_LightPosition[7].xyz);
          u_xlat16_3.x = max(u_xlat16_3.x, 0);
          u_xlat16_3.xyz = float3((u_xlat16_3.xxx * unity_LightColor[7].xyz));
          u_xlat16_3.xyz = float3((u_xlat16_3.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_3.xyz = float3(min(u_xlat16_3.xyz, float3(1, 1, 1)));
          u_xlat16_17 = max(u_xlat16_17, 0);
          u_xlat16_4.xyz = float3((float3(u_xlat16_17, u_xlat16_17, u_xlat16_17) * unity_LightColor[6].xyz));
          u_xlat16_4.xyz = float3((u_xlat16_4.xyz * float3(0.5, 0.5, 0.5)));
          u_xlat16_4.xyz = float3(min(u_xlat16_4.xyz, float3(1, 1, 1)));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz + u_xlat16_4.xyz));
          out_v.color.xyz = float3((u_xlat16_3.xyz + u_xlat16_2.xyz));
          out_v.color.xyz = float3(clamp(out_v.color.xyz, 0, 1));
          out_v.color.w = 1;
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _MainTex));
          out_v.vertex = UnityObjectToClipPos(float4(in_v.vertex, 0));
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat10_0;
      float3 u_xlat16_1;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat10_0.xyz = tex2D(_MainTex, in_f.texcoord.xy).xyz.xyz;
          u_xlat16_1.xyz = float3((u_xlat10_0.xyz * in_f.color.xyz));
          out_f.color.xyz = float3((u_xlat16_1.xyz + u_xlat16_1.xyz));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
    Pass // ind: 2, name: 
    {
      Tags
      { 
        "LIGHTMODE" = "VertexLM"
        "RenderType" = "Opaque"
      }
      LOD 80
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      // uniform float4 unity_LightmapST;
      uniform float4 _MainTex_ST;
      //uniform float4 unity_Lightmap_HDR;
      uniform sampler2D _MainTex;
      // uniform sampler2D unity_Lightmap;
      struct appdata_t
      {
          float3 vertex :POSITION0;
          float3 texcoord1 :TEXCOORD1;
          float3 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float2 texcoord :TEXCOORD0;
          float2 texcoord1 :TEXCOORD1;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float2 texcoord :TEXCOORD0;
          float2 texcoord1 :TEXCOORD1;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat01;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.texcoord.xy = float2(((in_v.texcoord1.xy * unity_LightmapST.xy) + unity_LightmapST.zw));
          out_v.texcoord1.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _MainTex));
          out_v.vertex = UnityObjectToClipPos(float4(in_v.vertex, 0));
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat16_0;
      float3 u_xlat10_0;
      float3 u_xlat16_1;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat16_0.xyz = UNITY_SAMPLE_TEX2D(unity_Lightmap, in_f.texcoord.xy).xyz.xyz;
          u_xlat16_1.xyz = float3((u_xlat16_0.xyz * unity_Lightmap_HDR.xxx));
          u_xlat10_0.xyz = tex2D(_MainTex, in_f.texcoord1.xy).xyz.xyz;
          out_f.color.xyz = float3((u_xlat16_1.xyz * u_xlat10_0.xyz));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
    Pass // ind: 3, name: ShadowCaster
    {
      Name "ShadowCaster"
      Tags
      { 
        "LIGHTMODE" = "SHADOWCASTER"
        "RenderType" = "Opaque"
        "SHADOWSUPPORT" = "true"
      }
      LOD 80
      Cull Off
      // m_ProgramMask = 6
      CGPROGRAM
      #pragma multi_compile SHADOWS_DEPTH
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4 _WorldSpaceLightPos0;
      //uniform float4 unity_LightShadowBias;
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4x4 unity_MatrixVP;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float3 normal :NORMAL0;
      };
      
      struct OUT_Data_Vert
      {
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 vertex :Position;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat02;
      float4 u_xlat1;
      float4 u_xlat2;
      float u_xlat6;
      float u_xlat9;
      int u_xlatb9;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat02.x = dot(in_v.normal.xyz, conv_mxt4x4_0(unity_WorldToObject).xyz);
          u_xlat02.y = dot(in_v.normal.xyz, conv_mxt4x4_1(unity_WorldToObject).xyz);
          u_xlat02.z = dot(in_v.normal.xyz, conv_mxt4x4_2(unity_WorldToObject).xyz);
          u_xlat02.xyz = float3(normalize(u_xlat02.xyz));
          u_xlat1 = mul(unity_ObjectToWorld, in_v.vertex);
          u_xlat2.xyz = float3((((-u_xlat1.xyz) * _WorldSpaceLightPos0.www) + _WorldSpaceLightPos0.xyz));
          u_xlat2.xyz = float3(normalize(u_xlat2.xyz));
          u_xlat9 = dot(u_xlat02.xyz, u_xlat2.xyz);
          u_xlat9 = (((-u_xlat9) * u_xlat9) + 1);
          u_xlat9 = sqrt(u_xlat9);
          u_xlat9 = (u_xlat9 * unity_LightShadowBias.z);
          u_xlat02.xyz = float3((((-u_xlat02.xyz) * float3(u_xlat9, u_xlat9, u_xlat9)) + u_xlat1.xyz));
          if((unity_LightShadowBias.z!=0))
          {
              u_xlatb9 = 1;
          }
          else
          {
              u_xlatb9 = 0;
          }
          float _tmp_dvx_17 = int(u_xlatb9);
          u_xlat02.xyz = float3(_tmp_dvx_17, _tmp_dvx_17, _tmp_dvx_17);
          u_xlat02 = mul(unity_MatrixVP, u_xlat02);
          u_xlat1.x = (unity_LightShadowBias.x / u_xlat02.w);
          u_xlat1.x = clamp(u_xlat1.x, 0, 1);
          u_xlat6 = (u_xlat02.z + u_xlat1.x);
          u_xlat1.x = max((-u_xlat02.w), u_xlat6);
          out_v.vertex.xyw = u_xlat02.xyw;
          u_xlat02.x = ((-u_xlat6) + u_xlat1.x);
          out_v.vertex.z = ((unity_LightShadowBias.y * u_xlat02.x) + u_xlat6);
          //return u_xlat0.xyz;
          //return u_xlat1.xyz;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          out_f.color = float4(0, 0, 0, 0);
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
