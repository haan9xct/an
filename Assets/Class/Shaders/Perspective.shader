Shader "Perspective"
{
  Properties
  {
    _PointDistance ("PointDistance", float) = 3
    _MainCam ("MainCam", 2D) = "white" {}
    _Emission ("Emission", Range(0, 1)) = 0
    _Color ("Color", Color) = (0,0,0,0)
    [HideInInspector] _texcoord ("", 2D) = "white" {}
    [HideInInspector] __dirty ("", float) = 1
  }
  SubShader
  {
    Tags
    { 
      "IsEmissive" = "true"
      "QUEUE" = "Geometry+0"
      "RenderType" = "Opaque"
    }
    Pass // ind: 1, name: FORWARD
    {
      Name "FORWARD"
      Tags
      { 
        "IsEmissive" = "true"
        "LIGHTMODE" = "FORWARDBASE"
        "QUEUE" = "Geometry+0"
        "RenderType" = "Opaque"
      }
      // m_ProgramMask = 6
      CGPROGRAM
      #pragma multi_compile DIRECTIONAL
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      #define conv_mxt4x4_3(mat4x4) float4(mat4x4[0].w,mat4x4[1].w,mat4x4[2].w,mat4x4[3].w)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4x4 unity_MatrixVP;
      uniform float _PointDistance;
      uniform float4 _texcoord_ST;
      //uniform float3 _WorldSpaceCameraPos;
      uniform float4 _Color;
      uniform float4 _MainCam_ST;
      uniform float _Emission;
      uniform sampler2D _MainCam;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float3 normal :NORMAL0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float2 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float4 texcoord2 :TEXCOORD2;
          float4 texcoord5 :TEXCOORD5;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float2 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float4 texcoord2 :TEXCOORD2;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      float u_xlat6;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat0 = (in_v.vertex.yyyy * conv_mxt4x4_1(unity_ObjectToWorld).zxyz);
          u_xlat0 = ((conv_mxt4x4_0(unity_ObjectToWorld).zxyz * in_v.vertex.xxxx) + u_xlat0);
          u_xlat0 = ((conv_mxt4x4_2(unity_ObjectToWorld).zxyz * in_v.vertex.zzzz) + u_xlat0);
          u_xlat0 = ((conv_mxt4x4_3(unity_ObjectToWorld).zxyz * in_v.vertex.wwww) + u_xlat0);
          u_xlat0 = (u_xlat0 + (-conv_mxt4x4_3(unity_ObjectToWorld)));
          u_xlat0.x = (u_xlat0.x + (-_PointDistance));
          u_xlat1.x = ((-_PointDistance) + (-0.5));
          u_xlat1.xy = float2((u_xlat0.xx / u_xlat1.xx));
          u_xlat1.z = 1;
          u_xlat0.xyz = float3((u_xlat0.yzw * u_xlat1.xyz));
          u_xlat1.xyz = float3((u_xlat0.yyy * conv_mxt4x4_1(unity_WorldToObject).xyz));
          u_xlat0.xyw = ((conv_mxt4x4_0(unity_WorldToObject).xyz * u_xlat0.xxx) + u_xlat1.xyz);
          u_xlat0.xyz = float3(((conv_mxt4x4_2(unity_WorldToObject).xyz * u_xlat0.zzz) + u_xlat0.xyw));
          u_xlat1 = (u_xlat0.yyyy * conv_mxt4x4_1(unity_ObjectToWorld));
          u_xlat1 = ((conv_mxt4x4_0(unity_ObjectToWorld) * u_xlat0.xxxx) + u_xlat1);
          u_xlat0 = ((conv_mxt4x4_2(unity_ObjectToWorld) * u_xlat0.zzzz) + u_xlat1);
          u_xlat1 = (u_xlat0 + conv_mxt4x4_3(unity_ObjectToWorld));
          out_v.texcoord2.xyz = float3(((conv_mxt4x4_3(unity_ObjectToWorld).xyz * in_v.vertex.www) + u_xlat0.xyz));
          out_v.vertex = mul(unity_MatrixVP, u_xlat1);
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _texcoord));
          u_xlat0.x = dot(in_v.normal.xyz, conv_mxt4x4_0(unity_WorldToObject).xyz);
          u_xlat0.y = dot(in_v.normal.xyz, conv_mxt4x4_1(unity_WorldToObject).xyz);
          u_xlat0.z = dot(in_v.normal.xyz, conv_mxt4x4_2(unity_WorldToObject).xyz);
          out_v.texcoord1.xyz = float3(normalize(u_xlat0.xyz));
          out_v.texcoord2.w = 0;
          out_v.texcoord5 = float4(0, 0, 0, 0);
          //return zxyz;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat0_d;
      float3 u_xlat1_d;
      float3 u_xlat2;
      float3 u_xlat10_2;
      float u_xlat6_d;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat0_d.xyz = float3(((-in_f.texcoord2.xyz) + _WorldSpaceCameraPos.xyz));
          u_xlat6_d = dot(u_xlat0_d.xyz, u_xlat0_d.xyz);
          u_xlat6_d = rsqrt(u_xlat6_d);
          u_xlat0_d.xyz = float3(((u_xlat0_d.xyz * float3(u_xlat6_d, u_xlat6_d, u_xlat6_d)) + float3(-1, 1, 0)));
          u_xlat0_d.xyz = float3(normalize(u_xlat0_d.xyz));
          u_xlat0_d.x = dot(in_f.texcoord1.xyz, u_xlat0_d.xyz);
          u_xlat0_d.x = ((u_xlat0_d.x * 0.5) + 0.5);
          u_xlat0_d.x = clamp(u_xlat0_d.x, 0, 1);
          u_xlat2.xy = float2(TRANSFORM_TEX(in_f.texcoord.xy, _MainCam));
          u_xlat10_2.xyz = tex2D(_MainCam, u_xlat2.xy).xyz.xyz;
          u_xlat2.xyz = float3((u_xlat10_2.xyz * _Color.xyz));
          u_xlat1_d.xyz = float3((u_xlat2.xyz * float3(_Emission, _Emission, _Emission)));
          out_f.color.xyz = float3(((u_xlat0_d.xxx * u_xlat2.xyz) + u_xlat1_d.xyz));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
    Pass // ind: 2, name: FORWARD
    {
      Name "FORWARD"
      Tags
      { 
        "IsEmissive" = "true"
        "LIGHTMODE" = "FORWARDADD"
        "QUEUE" = "Geometry+0"
        "RenderType" = "Opaque"
      }
      ZWrite Off
      Blend One One
      // m_ProgramMask = 6
      CGPROGRAM
      #pragma multi_compile POINT
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      #define conv_mxt4x4_3(mat4x4) float4(mat4x4[0].w,mat4x4[1].w,mat4x4[2].w,mat4x4[3].w)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4x4 unity_MatrixVP;
      uniform float4x4 unity_WorldToLight;
      uniform float _PointDistance;
      uniform float4 _texcoord_ST;
      //uniform float3 _WorldSpaceCameraPos;
      uniform float4 _Color;
      uniform float4 _MainCam_ST;
      uniform sampler2D _MainCam;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float3 normal :NORMAL0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float2 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float3 texcoord2 :TEXCOORD2;
          float3 texcoord3 :TEXCOORD3;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float2 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float3 texcoord2 :TEXCOORD2;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      float4 u_xlat2;
      float u_xlat10;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat0 = (in_v.vertex.yyyy * conv_mxt4x4_1(unity_ObjectToWorld).zxyz);
          u_xlat0 = ((conv_mxt4x4_0(unity_ObjectToWorld).zxyz * in_v.vertex.xxxx) + u_xlat0);
          u_xlat0 = ((conv_mxt4x4_2(unity_ObjectToWorld).zxyz * in_v.vertex.zzzz) + u_xlat0);
          u_xlat0 = ((conv_mxt4x4_3(unity_ObjectToWorld).zxyz * in_v.vertex.wwww) + u_xlat0);
          u_xlat0 = (u_xlat0 + (-conv_mxt4x4_3(unity_ObjectToWorld)));
          u_xlat0.x = (u_xlat0.x + (-_PointDistance));
          u_xlat1.x = ((-_PointDistance) + (-0.5));
          u_xlat1.xy = float2((u_xlat0.xx / u_xlat1.xx));
          u_xlat1.z = 1;
          u_xlat0.xyz = float3((u_xlat0.yzw * u_xlat1.xyz));
          u_xlat1.xyz = float3((u_xlat0.yyy * conv_mxt4x4_1(unity_WorldToObject).xyz));
          u_xlat0.xyw = ((conv_mxt4x4_0(unity_WorldToObject).xyz * u_xlat0.xxx) + u_xlat1.xyz);
          u_xlat0.xyz = float3(((conv_mxt4x4_2(unity_WorldToObject).xyz * u_xlat0.zzz) + u_xlat0.xyw));
          u_xlat1 = mul(unity_ObjectToWorld, float4(u_xlat0.xyz,1.0));
          out_v.vertex = mul(unity_MatrixVP, u_xlat1);
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _texcoord));
          u_xlat1.x = dot(in_v.normal.xyz, conv_mxt4x4_0(unity_WorldToObject).xyz);
          u_xlat1.y = dot(in_v.normal.xyz, conv_mxt4x4_1(unity_WorldToObject).xyz);
          u_xlat1.z = dot(in_v.normal.xyz, conv_mxt4x4_2(unity_WorldToObject).xyz);
          out_v.texcoord1.xyz = float3(normalize(u_xlat1.xyz));
          out_v.texcoord2.xyz = float3(((conv_mxt4x4_3(unity_ObjectToWorld).xyz * in_v.vertex.www) + u_xlat0.xyz));
          u_xlat0 = ((conv_mxt4x4_3(unity_ObjectToWorld) * in_v.vertex.wwww) + u_xlat0);
          u_xlat1.xyz = float3((u_xlat0.yyy * conv_mxt4x4_1(unity_WorldToLight).xyz));
          u_xlat1.xyz = float3(((conv_mxt4x4_0(unity_WorldToLight).xyz * u_xlat0.xxx) + u_xlat1.xyz));
          u_xlat0.xyz = float3(((conv_mxt4x4_2(unity_WorldToLight).xyz * u_xlat0.zzz) + u_xlat1.xyz));
          out_v.texcoord3.xyz = float3(((conv_mxt4x4_3(unity_WorldToLight).xyz * u_xlat0.www) + u_xlat0.xyz));
          //return zxyz;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat0_d;
      float3 u_xlat1_d;
      float3 u_xlat10_1;
      float u_xlat3;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat0_d.xyz = float3(((-in_f.texcoord2.xyz) + _WorldSpaceCameraPos.xyz));
          u_xlat3 = dot(u_xlat0_d.xyz, u_xlat0_d.xyz);
          u_xlat3 = rsqrt(u_xlat3);
          u_xlat0_d.xyz = float3(((u_xlat0_d.xyz * float3(u_xlat3, u_xlat3, u_xlat3)) + float3(-1, 1, 0)));
          u_xlat0_d.xyz = float3(normalize(u_xlat0_d.xyz));
          u_xlat0_d.x = dot(in_f.texcoord1.xyz, u_xlat0_d.xyz);
          u_xlat0_d.x = ((u_xlat0_d.x * 0.5) + 0.5);
          u_xlat0_d.x = clamp(u_xlat0_d.x, 0, 1);
          u_xlat1_d.xy = float2(TRANSFORM_TEX(in_f.texcoord.xy, _MainCam));
          u_xlat10_1.xyz = tex2D(_MainCam, u_xlat1_d.xy).xyz.xyz;
          u_xlat1_d.xyz = float3((u_xlat10_1.xyz * _Color.xyz));
          u_xlat0_d.xyz = float3((u_xlat1_d.xyz * u_xlat0_d.xxx));
          out_f.color.xyz = float3(u_xlat0_d.xyz);
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
