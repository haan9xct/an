Shader "Gloss"
{
  Properties
  {
    _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
    _MainTex ("Particle Texture", 2D) = "white" {}
    _InvFade ("Soft Particles Factor", Range(0.01, 3)) = 1
    _Mask ("Mask", 2D) = "white" {}
    _angle ("angle", float) = 45
    _scale ("scale", float) = 1
    [HideInInspector] _texcoord ("", 2D) = "white" {}
  }
  SubShader
  {
    Tags
    { 
      "IGNOREPROJECTOR" = "true"
      "PreviewType" = "Plane"
      "QUEUE" = "Transparent"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "PreviewType" = "Plane"
        "QUEUE" = "Transparent"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Blend One One
      ColorMask RGB
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _TintColor;
      uniform float _angle;
      uniform float _scale;
      uniform float4 _Mask_ST;
      uniform sampler2D _MainTex;
      uniform sampler2D _Mask;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.color = in_v.color;
          out_v.texcoord = in_v.texcoord;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float2 u_xlat0_d;
      float4 u_xlat16_0;
      float4 u_xlat10_0;
      float2 u_xlat1_d;
      float u_xlat10_1;
      float3 u_xlat2;
      float2 u_xlat3;
      float u_xlat6;
      float u_xlat9;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat0_d.x = (_angle * 0.0174532924);
          u_xlat1_d.x = cos(u_xlat0_d.x);
          u_xlat0_d.x = sin(u_xlat0_d.x);
          u_xlat2.z = u_xlat0_d.x;
          u_xlat3.xy = float2((in_f.texcoord.xy + float2(-0.5, (-0.5))));
          u_xlat2.y = u_xlat1_d.x;
          u_xlat2.x = (-u_xlat0_d.x);
          u_xlat1_d.y = dot(u_xlat3.xy, u_xlat2.xy);
          u_xlat1_d.x = dot(u_xlat3.xy, u_xlat2.yz);
          u_xlat0_d.xy = float2((u_xlat1_d.xy + float2(0.5, 0.5)));
          u_xlat0_d.xy = float2((u_xlat0_d.xy / float2(float2(_scale, _scale))));
          u_xlat6 = (in_f.color.x * _scale);
          u_xlat9 = (float(1) / _scale);
          u_xlat6 = (u_xlat6 * u_xlat9);
          u_xlat6 = (u_xlat6 * 3.4000001);
          u_xlat1_d.x = ((u_xlat9 * (-1.20000005)) + u_xlat6);
          u_xlat1_d.y = 0;
          u_xlat0_d.xy = float2((u_xlat0_d.xy + u_xlat1_d.xy));
          u_xlat10_0 = tex2D(_MainTex, u_xlat0_d.xy);
          u_xlat1_d.xy = float2(TRANSFORM_TEX(in_f.texcoord.xy, _Mask));
          u_xlat10_1 = tex2D(_Mask, u_xlat1_d.xy).w.x;
          u_xlat16_0 = (u_xlat10_0 * float4(u_xlat10_1, u_xlat10_1, u_xlat10_1, u_xlat10_1));
          u_xlat16_0 = (u_xlat16_0 * _TintColor);
          u_xlat16_0 = (u_xlat16_0 * _TintColor.wwww);
          u_xlat16_0 = (u_xlat16_0 * in_f.color.wwww);
          out_f.color = u_xlat16_0;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
