Shader "Custom/Beam"
{
  Properties
  {
    _TintColor ("Tint Color", Color) = (0.5,0.5,0.5,0.5)
    _MainTex ("Particle Texture", 2D) = "white" {}
  }
  SubShader
  {
    Tags
    { 
      "IGNOREPROJECTOR" = "true"
      "PreviewType" = "Plane"
      "QUEUE" = "Transparent"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "PreviewType" = "Plane"
        "QUEUE" = "Transparent"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Blend One One
      ColorMask RGB
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 color :COLOR0;
          float4 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      //float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.color = in_v.color;
          out_v.texcoord = in_v.texcoord;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float4 u_xlat0_d;
      float4 u_xlat16_1;
      float u_xlat2;
      float u_xlat4;
      int u_xlatb4;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat0_d.xy = float2(((-in_f.texcoord.xx) + float2(1, 0.0500000119)));
          u_xlat0_d.xy = float2((u_xlat0_d.yx * float2(19.9999962, 0.899999976)));
          u_xlat0_d.x = u_xlat0_d.x;
          u_xlat0_d.x = clamp(u_xlat0_d.x, 0, 1);
          u_xlat4 = ((u_xlat0_d.x * (-2)) + 3);
          u_xlat0_d.x = (u_xlat0_d.x * u_xlat0_d.x);
          u_xlat0_d.x = (u_xlat0_d.x * u_xlat4);
          u_xlat4 = ((in_f.texcoord.y * 2) + (-1));
          u_xlat4 = ((-abs(u_xlat4)) + 1);
          if((u_xlat4>=u_xlat0_d.y))
          {
              u_xlatb4 = 1;
          }
          else
          {
              u_xlatb4 = 0;
          }
          u_xlat2 = (u_xlat0_d.y * u_xlat0_d.y);
          u_xlat4 = u_xlatb4;
          u_xlat0_d.x = ((u_xlat2 * u_xlat4) + (-u_xlat0_d.x));
          u_xlat0_d.x = clamp(u_xlat0_d.x, 0, 1);
          u_xlat16_1 = (in_f.color.wwww * in_f.color);
          u_xlat0_d = (u_xlat0_d.xxxx * u_xlat16_1);
          out_f.color = u_xlat0_d;
          //return 1;
          //return float(0);
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
