﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteAlways]
public class ChangeName : MonoBehaviour
{

    private void OnEnable()
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).name = "head" + i;
        }
    }


}
