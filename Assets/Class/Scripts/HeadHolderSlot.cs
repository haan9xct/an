﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadHolderSlot : MonoBehaviour
{
    GameObject currentItem;

    public void EquipItem(string itemName)
    {
        if (currentItem != null)
        {
            Destroy(currentItem);
            currentItem = null;
        }
        if (itemName != null && itemName.Length > 0)
        {
            currentItem = Instantiate(Resources.Load<GameObject>("Skins/" + itemName), transform);
        }
    }
    public void DeSelect()
    {
        Destroy(currentItem);
    }
}
