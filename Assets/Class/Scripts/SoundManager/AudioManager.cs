﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    int playing;
    bool canPlay = true;
    [Header("SoundFX")]
    public AudioClip[] soundsFX;
    [Header("Music")]
    public AudioClip[] musics;
    public static AudioManager Instance;
    public AudioSource musicSource;
    public AudioSource soundSource;

    public int count4PlayFireEndgame = 0;
    public int count4MaxPlaySametime = 0;

    public bool canPlaymanyFx = true;

    private void Awake()
    {
        Instance = this;
        canPlaymanyFx = true;
    }

    public void Play(string name, bool isloop = false)
    {
        if (GameMaster.Instance.isTurnOnAudio == false) return;
        AudioClip s = Array.Find(musics, sound => sound.name == name);
        if (s != null)
        {
            musicSource.clip = s;
            musicSource.loop = isloop;
            musicSource.Play();
        }
    }
    public void Stop(string name)
    {
        musicSource.Stop();
    }
    public void PlayOneShot(string name, float volume)
    {
        if (GameMaster.Instance.isTurnOnAudio == false) return;
        AudioClip s = Array.Find(soundsFX, sound => sound.name == name);
        if (s != null)
        {
            soundSource.clip = s;
            soundSource.PlayOneShot(s, volume);
        }
    }
    public void PlayOneShotByClip(AudioClip clip, float volume)
    {
        if (GameMaster.Instance.isTurnOnAudio == false) return;
        if(clip != null)
        {
            soundSource.clip = clip;
            soundSource.PlayOneShot(clip,volume);
        }
    }
    public void SetVolume(float volume)
    {
        musicSource.volume = volume;
    }
    public void waitPlayMenyEffect() {
        canPlaymanyFx = false;
        StartCoroutine(wait4CountPlayFireEndGame());
    }
    IEnumerator wait4CountPlayFireEndGame() {
        yield return new WaitForSeconds(0.1f);
        canPlaymanyFx = true;
        yield return new WaitForSeconds(0.15f);
        if (count4PlayFireEndgame > 0) {
            count4PlayFireEndgame--;
        }
    }
  
    public void PauseAudio()
    {
        // AudioListener.pause = true;
    }
    public void ResumeAudio()
    {
        // AudioListener.pause = false;
    }
   
}
