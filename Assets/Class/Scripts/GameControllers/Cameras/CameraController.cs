﻿using System.Collections;
using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("References")]
    public CinemachineVirtualCamera cinemachineVirtual;
    public Transform followingTarget;
    public AlliesController alliesController;
    public Camera mainCamera;
    [Header("Status")]
    private bool rotating = false;

    private Vector3 rotation;
  

    private void FixedUpdate()
    {
        if (rotating)
        {
            cinemachineVirtual.transform.rotation = Quaternion.Euler(rotation);
        }
    }


    public void OnAlliesMoving()
    {
        // Lock y-axis
        if (alliesController.AlliesCrowd.Count > 0)
        {
            followingTarget.position = new Vector3(alliesController.CenterChar.position.x, 0f, alliesController.CenterChar.position.z);
        }

    }


    public void RecordBattleScene()
    {
        // TODO: Rotate camera to x = 15 degree
        rotating = true;
        rotation = cinemachineVirtual.transform.rotation.eulerAngles;
        DOTween.To(() => rotation, x => rotation = x, new Vector3(23f, 0f, 0f), 1.5f).OnKill(() =>
        {
            rotating = false;
        });
        followingTarget.DOMove(new Vector3(0f, 10f, followingTarget.position.z + 15f), 1.5f);
    }


    public void RecordWinningScene()
    {
        followingTarget.DOMove(new Vector3(0f, -10, followingTarget.position.z - 8f), 3.5f).OnStart(() => cinemachineVirtual.transform.DORotate(new Vector3(-10, 0, 0), 4f));
    }


    public void RecordLoseScene()
    {
        followingTarget.DOMove(new Vector3(0f, -5, followingTarget.position.z + 12f), 2.0f);
        cinemachineVirtual.transform.DORotate(new Vector3(10, 0, 0), 2f);
    }


    public void ShottingGateScene()
    {
        followingTarget.DOMove(new Vector3(0f, 0f, followingTarget.position.z + 25), 3f).OnStart(() => cinemachineVirtual.transform.DORotate(new Vector3(20, 0, 0), 3f));
    }


   


    IEnumerator LerpFOV()
    {
        float time = 0;
        while (time < 2)
        {
            time += Time.deltaTime;
            cinemachineVirtual.m_Lens.FieldOfView = Mathf.Lerp(75, 90, time);
            yield return null;
        }
    }


   
    public void TurnCamFollow(float angle)
    {
        cinemachineVirtual.transform.DORotate(new Vector3(25, angle, 0), 1f);
        Vector3 offset = cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;
    }

    public void Reset()
    {
        cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 0;
        cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_YDamping = 0;
        cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0;
        followingTarget.position = Vector3.zero;
    }
}
