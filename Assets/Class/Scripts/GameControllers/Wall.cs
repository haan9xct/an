﻿using PathologicalGames;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] GameObject impactWallFX;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("AllyBullet"))
        {
            Vector3 posSpawn = other.transform.position;
            Transform impact = PoolManager.Pools["Game"].Spawn(impactWallFX, posSpawn, Quaternion.identity);
            PoolManager.Pools["Game"].Despawn(impact, 3f);
            PoolManager.Pools["Game"].Despawn(other.transform);
        }
    }
}
