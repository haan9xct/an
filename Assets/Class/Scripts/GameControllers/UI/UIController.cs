﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using mygame.sdk;

public class UIController : MonoBehaviour
{
    [Header("Panel UI")]

    [SerializeField] GameObject endgamePanel;
    [SerializeField] GameObject ingameUI;
    [SerializeField] GameObject menuUI;
    [SerializeField] GameObject winPanel;
    [SerializeField] GameObject losePanel;
    public BattlePanel battlePanel;
    [Header("Display")]
    public GameObject tutorial;
    public GameObject panelStart;
    public Camera mainCamera;
    [SerializeField] CanvasScaler canvasCam;
    public static UIController Instance;
    public bool isWait4NewPlay = false;
    public Transform btOtherGame;
    public bool isWin;
    private void Awake()
    {
        if (Instance == null) Instance = this;
        if (SdkUtil.isiPad())
        {
            canvasCam.matchWidthOrHeight = 1;
        }
        else
        {
            canvasCam.matchWidthOrHeight = 0.5f;
        }
#if UNITY_IOS
        btRestore.gameObject.SetActive(true);
#endif
        

    }

    private void Start()
    {
        setSafeArea();
        OpenMenuUI();
    }

    private void setSafeArea()
    {
#if UNITY_ANDROID

#else
        float bot = Screen.height - Screen.safeArea.yMax;
        Transform tf = transform.Find("MainMenu");
        tf.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, bot / 2);
        tf.GetComponent<RectTransform>().sizeDelta = new Vector2(0, -bot);
        tf = transform.Find("EndgameUI");
        tf.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, bot / 2);
        tf.GetComponent<RectTransform>().sizeDelta = new Vector2(0, -bot);
#endif

        float perr = (float)Screen.height / (float)Screen.width;
        if (perr < 1.4f)
        {
            // GridLayoutGroup gid = transform.Find("MainMenu").Find("GroupBtUpdate").GetComponent<GridLayoutGroup>();
            // RectTransform rtf = gid.GetComponent<RectTransform>();
            // float h = (rtf.anchorMax.y - rtf.anchorMin.y) * Screen.height / perr;
            // float w = h * 318.0f / 408.0f;
            // gid.cellSize = new Vector2(w, h);
        }
    }
 
    public void OpenBattlePanel()
    {
        ingameUI.SetActive(false);
        battlePanel.gameObject.SetActive(true);
    }
        
    public void OpenMenuUI()
    {
        RenderSettings.fog = true;
        RenderSettings.fogStartDistance = 50;
        RenderSettings.fogEndDistance = 90;
        GameMaster.Instance.canOpenGift = true;
        panelStart.SetActive(true);
        GameMaster.gameStates = GameStates.MENU;
        GameMaster.Instance.cameraController.mainCamera.orthographic = false;
        GameMaster.Instance.directionLight.transform.rotation = Quaternion.Euler(55, -60, 0);
        mainCamera.GetComponent<CameraController>().followingTarget.position = new Vector3(0, 0, 0);
        menuUI.SetActive(true);
        mainCamera.gameObject.SetActive(true);
        ingameUI.SetActive(false);
        battlePanel.gameObject.SetActive(false);
        winPanel.SetActive(false);
        losePanel.SetActive(false);

        if ((PlayerPrefsUtil.Level == PlayerPrefsUtil.levelShowRating1 || PlayerPrefsUtil.Level == PlayerPrefsUtil.levelShowRating2) && PlayerPrefsUtil.isShowRate == 0)
        {
            PlayerPrefsUtil.isShowRate = 1;
        }
        if (PlayerPrefsUtil.Level == (PlayerPrefsUtil.levelShowRating2 - 1))
        {
            PlayerPrefsUtil.isShowRate = 0;
        }
    }
   
    private IEnumerator readyNewPlay()
    {
        yield return new WaitForSeconds(0.2f);
        isWait4NewPlay = false;
    }

    public void ReloadMap()
    {
        GameMaster.gameStates = GameStates.MENU;
        LevelController.Instance.Prepe4Play();
        GameMaster.Instance.Reset();
        isWait4NewPlay = true;
        StartCoroutine(readyNewPlay());
    }

    public void OpenIngameUI()
    {
        if (PlayerPrefsUtil.memIdxbonusMap > -2)
        {
            GameMaster.gameStates = GameStates.INGAME;
            menuUI.SetActive(false);
            ingameUI.SetActive(true);
        }
        else
        {
            GameMaster.gameStates = GameStates.INGAME;
            menuUI.SetActive(false);
            ingameUI.SetActive(true);
        }
    }

    
   
}
