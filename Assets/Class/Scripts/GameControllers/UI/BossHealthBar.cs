﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{

    public static BossHealthBar Instance;
    [SerializeField] GameObject healthBarPrefab;
    public float currentHealth;
    public float maxHealth;
    bool isDead;
    public GameObject hpBar;
    private void OnEnable()
    {

    }
    void Start()
    {
        Instance = this;
        isDead = false;
        //hpBar.SetActive(false);
    }
    public string GetInfoHP()
    {
        return currentHealth.ToString() + " : " + maxHealth.ToString();
    }
    private void Update()
    {
        if (hpBar == null)
        {
            hpBar = Instantiate(healthBarPrefab, transform.position + new Vector3(0, 8, 0), Quaternion.Euler(25, 0, 0));
            hpBar.SetActive(false);
        }
        else
        {

            hpBar.transform.position = transform.position + new Vector3(0, 8, 0);
           hpBar.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().fillAmount = (float)currentHealth / (float)maxHealth;
            int hpdis = (int)currentHealth;
            if (currentHealth > 0 && hpdis == 0)
            {
                hpdis = 0;
            hpBar.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);

            }
            hpBar.transform.GetChild(1).GetComponent<TextMeshPro>().text = hpdis.ToString();
        }

    }
    public void SetActiveHPbar()
    {
        hpBar.SetActive(true);
        maxHealth = 100f;
        currentHealth = 100f;
    }
    private void OnDisable()
    {
        Destroy(hpBar);
    }
}
