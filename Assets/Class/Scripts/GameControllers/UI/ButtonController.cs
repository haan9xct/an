﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
public class ButtonController : Button
{
    public Action CallFunc;
    Image buttonImage;
    Color defaultColor;
    Color colorChange;
    public ButtonType buttonType;
    protected override void OnDisable()
    {
        if (buttonImage != null) buttonImage.color = defaultColor;
    }
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (buttonImage != null) buttonImage.color = colorChange;
        if (CallFunc != null) CallFunc();

    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (buttonImage != null) buttonImage.color = defaultColor;
    }
    protected override void Start()
    {
        buttonImage = GetComponent<Image>();
        if (buttonImage != null)
        {
            defaultColor = buttonImage.color;
            colorChange = new Color(0.8f, 0.8f, 0.8f);
            if (buttonType == ButtonType.ADs) transform.DOScale(1.1f, 1.2f).SetLoops(-1, LoopType.Yoyo);
        }
    }
}
public enum ButtonType
{
    Normal, ADs
}