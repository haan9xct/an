﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EndgameStates
{
	PRE_BATTLE,
	DEPLOYMENT_STARTED,
	FIGHTING,
}


public class EndgameController : MonoBehaviour
{
	public static EndgameController Instance;
	[Header("References")]
	public BattleController battleController;

	[Header("State parameters")]
	public bool isBonusScene;


	private void Awake()
	{
		Instance = this;
	}


	public void OnPreBattle()
	{
		GameMaster.gameStates = GameStates.ENDGAME;
		GameMaster.endgameStates = EndgameStates.PRE_BATTLE;
			battleController.OnPreBattle();


	}


	public void OnBattleStart(Transform allyCrowdAnchor)
	{
		GameMaster.endgameStates = EndgameStates.DEPLOYMENT_STARTED;
		battleController.UpdateTargetsAlly();
		battleController.UpdateTargetsEnemy();
		battleController.OnBattleStart(allyCrowdAnchor);
	}

}
