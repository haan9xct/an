﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndgameDetector : MonoBehaviour
{
    [Header("Trigger types")]
    public bool prebattleTrigger;
    public bool battleTrigger;

	[Header("Anchors")]
	public Transform allyCrowdAnchor;

	private void Start()
	{
	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.name);
		if (other.transform.CompareTag(Utilities.NEUTRAL_STANDARD_COLLIDER_TAG))
		{
			GetComponent<BoxCollider>().enabled = false;

			if (prebattleTrigger)
			{
				 Debug.Log("Star ===========");
				EndgameController.Instance.OnPreBattle();
			}
			else
			{
				EndgameController.Instance.OnBattleStart(allyCrowdAnchor);
			}
		}
	}
}
