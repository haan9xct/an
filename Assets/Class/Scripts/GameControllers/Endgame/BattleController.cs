using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using mygame.sdk;
using PathologicalGames;
public class BattleController : MonoBehaviour
{
    public static BattleController Instance;
    [Header("Testing")]
    public bool syncDancing = false;

    [Header("References")]
    public CameraController cameraController;
    public GameObject UIWin;
    public string pathRes;


    [Header("Anchors")]
    public Transform leftSpawner;
    public Transform rightSpawner;
    public Transform deployingAnchorEnemy;
    public Transform[] gatePath;
    private Transform deployingAnchorAlly;
    public Transform gateAnchor;
    public Transform kingSpawnAnchor;
    public Transform heroDanceAnchor;

    [Header("Effect")]
    public GameObject gateExplosion;
    public GameObject winningEffect;
    public GameObject gate;

    [Header("Ally")]
    public float rowSizeAlly = 12.5f;

    private List<BaseEnemyObj> allyCrowd = new List<BaseEnemyObj>();
    private List<Vector3> deployingPositionsAlly = new List<Vector3>();
    private Dictionary<Transform, Transform> targetsMapAlly = new Dictionary<Transform, Transform>();
    private List<BaseEnemyObj> listAllyfighted = new List<BaseEnemyObj>();

    [Header("Enemy")]
    public BaseEnemyObj bossPrefab;
    public float rowSizeEnemy = 12.5f;
    private List<BaseEnemyObj> enemyCrowd = new List<BaseEnemyObj>();
    public List<BaseEnemyObj> listEnemyfighted = new List<BaseEnemyObj>();
    private List<Vector3> deployingPositionsEnemy = new List<Vector3>();
    private Dictionary<Transform, Transform> targetsMapEnemy = new Dictionary<Transform, Transform>();

    public List<BaseEnemyObj> EnemyCrowd { get => enemyCrowd; }

    private List<BaseEnemyObj> listIdx4fightingAlly = new List<BaseEnemyObj>();
    private List<BaseEnemyObj> listIdx4fightingEnemy = new List<BaseEnemyObj>();

    private bool isPreBattle = false;

    private BossHealthBar hpBoss = null;

    private float totalPowerAllies = 0;

    private float totalPowerEnemies = 0;

    private float totalTimeBattle = 0;

    #region Getters
    public float GetAllyCrowdHP()
    {
        float HP = 0f;
        for (int i = 0; i < allyCrowd.Count; i++)
        {
            HP += allyCrowd[i].blood;
        }

        return HP;
    }
    public float GetAllyCrowdDPS()
    {
        float DPS = 0f;
        for (int i = 0; i < allyCrowd.Count; i++)
        {
            DPS += allyCrowd[i].power;
        }

        return DPS;
    }
    public float GetEnemyCrowdHP()
    {
        float HP = 0f;
        for (int i = 0; i < enemyCrowd.Count; i++)
        {
            HP += enemyCrowd[i].blood;
        }

        return HP;
    }
    public float GetEnemyCrowdDPS()
    {
        float DPS = 0f;
        for (int i = 0; i < enemyCrowd.Count; i++)
        {
            DPS += enemyCrowd[i].power;
        }

        return DPS;
    }
    #endregion


    private void Start()
    {

        //        cameraController = LevelController.Instance.alliesController.gameMaster.cameraController;
        Instance = this;
        gateExplosion.SetActive(false);
        winningEffect.SetActive(false);
        gate.SetActive(true);
        BaseEnemyObj enemy = Instantiate(bossPrefab, deployingAnchorEnemy.position + new Vector3(0f, 0f, 10f), Quaternion.Euler(new Vector3(0f, 180f, 0f)), transform);
        BaseEnemyObj boss = new BaseEnemyObj();
        boss.blood = 100;
        boss.power = 110;
        boss.armor = 0;
        enemy.copyData(boss);
        enemyCrowd.Add(enemy);
        listEnemyfighted.Add(enemy);
    }


    private void Update()
    {
        //  BaseEnemyObj enemy = Instantiate(bossPrefab, deployingAnchorEnemy.position + new Vector3(0f, 0f, 10f), Quaternion.Euler(new Vector3(0f, 180f, 0f)), transform);
        // enemy.copyData(data);
        //enemyCrowd.Add(enemy);

        if (GameMaster.gameStates == GameStates.ENDGAME && GameMaster.endgameStates == EndgameStates.FIGHTING)
        {
            Fight();
            CheckLosing();
        }
    }




    private void OnDisable()
    {
        // allyCrowd.Clear();
        // for (int i = 0; i < EnemyCrowd.Count; i++)
        // {
        //     if (enemyCrowd[i].typeEnemy == 0)
        //     {
        //         PoolManager.Pools["Game"].Despawn(enemyCrowd[i].transform);
        //     }
        // }
        // enemyCrowd.Clear();
    }

    public void OnPreBattle()
    {
        StartCoroutine(LerpFog());
        isEdie = 0;
        isAdie = 0;

        listIdx4fightingAlly.Clear();
        listIdx4fightingEnemy.Clear();
        allyCrowd.Clear();
        for (int i = 0; i < LevelController.Instance.alliesController.AlliesCrowd.Count; i++)
        {
            BaseEnemyObj ally = LevelController.Instance.alliesController.AlliesCrowd[i].GetComponent<BaseEnemyObj>();
            if (!ally.gameObject.name.StartsWith("Main Player"))
            {


                ally.blood = LevelController.Instance.alliesInfo.blood;
                ally.power = LevelController.Instance.alliesInfo.power;
                ally.armor = LevelController.Instance.alliesInfo.armor;
            }
            allyCrowd.Add(ally);
        }
        // Spawn enemies
        // Debug.Log("count ally=" + allyCrowd.Count);

        hpBoss = BossHealthBar.Instance;
        if (hpBoss != null)
        {
            hpBoss.SetActiveHPbar();
            hpBoss.maxHealth = 100;
            hpBoss.currentHealth = enemyCrowd[0].blood;

        }
        //Debug.Log("hp Boss=" + hpBoss.GetInfoHP());

    }


    IEnumerator LerpFog()
    {
        float time = 0;
        while (time < 2)
        {
            time += Time.deltaTime;
            RenderSettings.fogStartDistance = Mathf.Lerp(RenderSettings.fogStartDistance, 100, time);
            RenderSettings.fogEndDistance = Mathf.Lerp(RenderSettings.fogEndDistance, 130, time);
            yield return null;
        }

    }


    public void OnBattleStart(Transform allyCrowdanchor)
    {

        if (!isPreBattle)
        {
            isPreBattle = true;

            AudioManager.Instance.Stop("Running");
            AudioManager.Instance.Play("BeforeTheFight");
            allyCrowd.Clear();
            listIdx4fightingAlly.Clear();
            for (int i = 0; i < LevelController.Instance.alliesController.AlliesCrowd.Count; i++)
            {
                BaseEnemyObj ally = LevelController.Instance.alliesController.AlliesCrowd[i].GetComponent<BaseEnemyObj>();
                allyCrowd.Add(ally);
                listIdx4fightingAlly.Add(ally);
            }
            this.deployingAnchorAlly = allyCrowdanchor;
            GenDeployingAllyPositions();

            cameraController.RecordBattleScene();

            // Deploy allies
            for (int i = deployingPositionsAlly.Count - 1; i >= 0; i--)
            {
                Transform deployingChar = allyCrowd[i].transform;
                float movingTime = Vector3.Distance(deployingPositionsAlly[i], deployingChar.position) / Utilities.CROWDS_STANDARD_MOVING_SPEED;
                deployingChar.DOMove(deployingPositionsAlly[deployingPositionsAlly.Count - 1 - i], movingTime).OnStart(() => deployingChar.Rotate(Vector3.forward)).OnKill(() =>
                  {
                      deployingChar.GetComponent<CharacterAnimator>().OnShoot1Trigger(false);
                  });
            }

            // Deploy enemies
            GenDeployingPositionsEnemy();

            for (int i = 0; i < deployingPositionsEnemy.Count; i++)
            {
                Transform deployingChar = enemyCrowd[i].transform;

                // 0.65 = relative ratio between allies moving distance and enemies moving distance
                float movingTime = Vector3.Distance(deployingPositionsEnemy[i], deployingChar.position) / (Utilities.CROWDS_STANDARD_MOVING_SPEED * 0.65f);
                deployingChar.GetComponent<CharacterAnimator>().OnLocomotionTrigger(1);
                deployingChar.DOMove(deployingPositionsEnemy[i], movingTime).OnKill(() =>
                {
                    deployingChar.GetComponent<CharacterAnimator>().OnShoot1Trigger(false);
                });
            }


            // Start crowds dmg calculation
            StartCoroutine(HandleDPS());
        }
    }


    private IEnumerator HandleDPS()
    {
        // Shooting direction adjustment
        UpdateTargetsAlly();
        UpdateTargetsEnemy();

        yield return new WaitForSeconds(2.1f);
        UIController.Instance.OpenBattlePanel();
        AudioManager.Instance.Stop("BeforeTheFight");
        totalPowerAllies = GetAllyCrowdDPS();
        totalPowerEnemies = GetEnemyCrowdDPS();

        GameMaster.endgameStates = EndgameStates.FIGHTING;

        //listEnemyfighted.Clear();
        listAllyfighted.Clear();

        for (int i = (enemyCrowd.Count - 1); i >= 0; i--)
        {
            if (enemyCrowd[i] == null || enemyCrowd[i].gameObject == null || enemyCrowd[i].transform == null)
            {
                enemyCrowd.RemoveAt(i);
            }
        }

        UpdateTargetsAlly();
        UpdateTargetsEnemy();

        totalTimeBattle = 0;
        if (PlayerPrefsUtil.TypeBattleFight == 1 && allyCrowd.Count > 0 && enemyCrowd.Count > 0)
        {
            maxkCurr4Fight = maxk4Fight;
            minkCurr4Fight = mink4Fight;
            float TmpP = totalPowerEnemies;
            if (TmpP < totalPowerAllies)
            {
                TmpP = totalPowerAllies;
            }
            float tminfight = 4;
            if (PlayerPrefsUtil.IsSecondPlay == 0)
            {
                tminfight = 5;
            }
            else
            {
                if (PlayerPrefsUtil.Level % 3 == 0)
                {
                    if (EnemyCrowd[0].blood >= 60)
                    {
                        tminfight = 4.5f;
                    }
                    else
                    {
                        tminfight = 3;
                    }
                }
                else
                {
                    if (EnemyCrowd.Count >= 6)
                    {
                        tminfight = 4.5f;
                    }
                    else if (EnemyCrowd.Count >= 3)
                    {
                        tminfight = 3f;
                    }
                    else
                    {
                        tminfight = 1.5f;
                    }
                }
            }
            float taB = GetAllyCrowdHP();
            float teB = GetEnemyCrowdHP();
            float aar = allyCrowd[0].armor;
            if (allyCrowd.Count > 1)
            {
                aar = allyCrowd[1].armor;
            }
            if (aar > totalPowerEnemies * 0.3f)
            {
                aar = totalPowerEnemies * 0.3f;
            }
            float tallAllyDie = taB / ((totalPowerEnemies - aar) * LevelController.Instance.kSubBlood);
            float kmaxa = tallAllyDie / tminfight;
            float ear = enemyCrowd[0].armor;
            if (ear > totalPowerAllies * 0.3f)
            {
                ear = totalPowerAllies * 0.3f;
            }
            float tallEnemyDie = teB / ((totalPowerAllies - ear) * LevelController.Instance.kSubBlood);
            float kmaxe = tallEnemyDie / tminfight;
            float kmax = kmaxa;
            if (kmax > kmaxe)
            {
                kmax = kmaxe;
            }
            float k4Fightbegin = minkCurr4Fight + (maxkCurr4Fight - minkCurr4Fight) * (TmpP - minp4Fight) / (maxp4Fight - minp4Fight);
            if (kmax < ((k4Fightbegin + minkCurr4Fight) / 2))
            {
                if (kmax > minkCurr4Fight)
                {
                    maxkCurr4Fight = 2 * kmax - minkCurr4Fight;
                }
                else
                {
                    minkCurr4Fight = 4.0f * kmax / 5.0f;
                    maxkCurr4Fight = 2 * kmax - minkCurr4Fight;
                }
            }
        }
        for (int i = 0; i < 5; i++)
        {
            AddListAllyfighted(1);
            AddListEnemyfighted(1);
            yield return new WaitForSeconds(0.4f);
        }
    }

    float maxp4Fight = 800;
    float minp4Fight = 50;
    float maxk4Fight = 0.75f;
    float mink4Fight = 0.45f;
    float maxkCurr4Fight = 0.8f;
    float minkCurr4Fight = 0.45f;

    int isEdie = 0;
    int isAdie = 0;
    private void Fight()
    {
        int ne = listEnemyfighted.Count;
        int na = listAllyfighted.Count;
        float pe = 1;
        float pa = 1;
        float k4Fight = 1.0f;
        if (PlayerPrefsUtil.TypeBattleFight == 1)
        {
            pe = totalPowerEnemies;
            pa = totalPowerAllies;
            float tmpPa = GetAllyCrowdDPS();
            float tmpPe = GetEnemyCrowdDPS();
            float TmpP = tmpPa;
            if (TmpP < tmpPe)
            {
                TmpP = tmpPe;
            }

            if (TmpP > maxp4Fight)
            {
                TmpP = maxp4Fight;
            }
            else if (TmpP < minp4Fight)
            {
                TmpP = minp4Fight;
            }
            k4Fight = minkCurr4Fight + (maxkCurr4Fight - minkCurr4Fight) * (TmpP - minp4Fight) / (maxp4Fight - minp4Fight);
            if (na > 0)
            {
                pe = pe / (float)na;
            }
            if (ne > 0)
            {
                pa = pa / (float)ne;
            }
        }
        else
        {
            if (na > 0)
            {
                pe = GetEnemyCrowdDPS() / (float)na;
            }
            if (ne > 0)
            {
                pa = GetAllyCrowdDPS() / (float)ne;
            }
        }

        totalTimeBattle += Time.deltaTime;

        for (int i = (ne - 1); i >= 0; i--)
        {
            float artmp = listEnemyfighted[i].armor;
            if (pa * 0.6f < artmp)
            {
                artmp = pa * 0.6f;
            }
            float dblood = (pa - artmp) * Time.deltaTime * LevelController.Instance.kSubBlood * k4Fight;
            listEnemyfighted[i].blood -= dblood*15;
            listEnemyfighted[0].GetComponent<BossAnimator>().Attack();
            if (hpBoss != null)
            {
                hpBoss.SetActiveHPbar();
                hpBoss.maxHealth = 100f;
                hpBoss.currentHealth = (int)listEnemyfighted[0].blood;

                if (listEnemyfighted[0].blood <= 0)
                {
                    listEnemyfighted[0].GetComponent<BossAnimator>().Dead();
                    Debug.Log("check Health");
                    Destroy(hpBoss.hpBar);
                }
            }
            if (listEnemyfighted[i].blood <= 0)
            {
                isEdie++;
                if (listEnemyfighted[0].GetComponent<BossAnimator>() != null)
                {
                    listEnemyfighted[0].GetComponent<BossAnimator>().OnDeadTrigger();
                    enemyCrowd.Remove(listEnemyfighted[0]);
                    listEnemyfighted.RemoveAt(0);
                }
                else
                {
                    listEnemyfighted[i].GetComponent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Enemy, true);
                    enemyCrowd.Remove(listEnemyfighted[i]);
                    listEnemyfighted.RemoveAt(i);
                }
            }
        }


        for (int i = (na - 1); i >= 0; i--)
        {
            float artmp = listAllyfighted[i].armor;

            if (pe * 0.6f < artmp)
            {
                artmp = pe * 0.6f;
            }
            float dblood = (pe - artmp) * Time.deltaTime * LevelController.Instance.kSubBlood * k4Fight;
            listAllyfighted[i].blood -= dblood ;
            //Debug.Log("Player = "+ listAllyfighted[i].blood);
            if (listAllyfighted[i].blood <= 0)
            {
                if (enemyCrowd.Count > 0 || allyCrowd.Count > 1)
                {
                    isAdie++;
                    if (!listAllyfighted[i].name.StartsWith("Main"))
                    {
                        listAllyfighted[i].GetComponent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Allied, true);
                    }
                    else
                    {
                        listAllyfighted[i].GetComponent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Allied, false);
                    }
                    allyCrowd.Remove(listAllyfighted[i]);
                    LevelController.Instance.alliesController.AlliesCrowd.Remove(listAllyfighted[i].transform);
                    listAllyfighted.RemoveAt(i);
                }
            }
        }
        if (isEdie > 0)
        {
            UpdateTargetsAlly();
            AddListEnemyfighted(isEdie);
            float ab = 0;
            if (listAllyfighted.Count > 0)
            {
                ab = listAllyfighted[0].blood;
            }
        }
        if (isAdie > 0)
        {
            UpdateTargetsEnemy();
            AddListAllyfighted(isAdie);
            float eb = 0;
            if (listEnemyfighted.Count > 0)
            {
                eb = listEnemyfighted[0].blood;
            }
        }
    }


    private void CheckLosing()
    {
        if (enemyCrowd.Count == 0)
        {
            GameMaster.gameStates = GameStates.MENU;
            UIController.Instance.battlePanel.gameObject.SetActive(false);
            string lv4log = $"level_{PlayerPrefsUtil.Level:000}_win";
            Dictionary<string, string> paramlog = new Dictionary<string, string>();
            LevelController.Instance.OnWinning();
            InvadeCastle();
        }
        else if (allyCrowd.Count == 0)
        {
            GameMaster.gameStates = GameStates.MENU;
            UIController.Instance.battlePanel.gameObject.SetActive(false);
            string lv4log = $"level_{PlayerPrefsUtil.Level:000}_lose";
            Dictionary<string, string> paramlog = new Dictionary<string, string>();
            paramlog.Add("level_fail", PlayerPrefsUtil.Level.ToString());
            AudioManager.Instance.PlayOneShot("LoseWithLaugh", 1f);
            LevelController.Instance.OnFightingLose(enemyCrowd);
            cameraController.RecordLoseScene();
            StartCoroutine(EnemiesDance());
        }
    }
    //Ally
    private void AddListAllyfighted(int count)
    {
        if (PlayerPrefsUtil.Level % 3 == 0)
        {
            if (PlayerPrefsUtil.Level <= 9)
            {
                if (listAllyfighted.Count > (PlayerPrefsUtil.Level / 3))
                {
                    return;
                }
            }
        }
        else
        {
            if (listAllyfighted.Count > listIdx4fightingEnemy.Count)
            {
                return;
            }
        }
        for (int i = 0; i < count; i++)
        {
            if (listIdx4fightingAlly.Count > 0)
            {
                int idx;
                if (listIdx4fightingAlly.Count > 4)
                {
                    idx = Random.Range(1, listIdx4fightingAlly.Count);
                }
                else
                {
                    idx = Random.Range(0, listIdx4fightingAlly.Count);
                }
                listAllyfighted.Add(listIdx4fightingAlly[idx]);
                listIdx4fightingAlly.RemoveAt(idx);
            }
        }
    }
    public void UpdateTargetsAlly()
    {
        if (allyCrowd.Count > 0 && enemyCrowd.Count > 0)
        {
            targetsMapAlly.Clear();
            for (int i = 0; i < allyCrowd.Count; i++)
            {
                BaseEnemyObj enemy = null;
                if (enemyCrowd.Count > 0)
                {
                    enemy = enemyCrowd[Random.Range(0, enemyCrowd.Count)];
                }
                if (enemy != null)
                {
                    targetsMapAlly.Add(allyCrowd[i].transform, enemy.transform);
                }
            }
            foreach (var pair in targetsMapAlly)
            {
                pair.Key.DOLookAt(pair.Value.position, Utilities.ENDGAME_AIMING_TIME);
            }
        }
    }

    private void GenDeployingAllyPositions()
    {
        int currentRow = 0;
        int currentCol = 0;
        deployingPositionsAlly.Clear();

        while (deployingPositionsAlly.Count < allyCrowd.Count)
        {
            float newX = 0f;
            if (currentRow % 2 == 1)
            {
                if (currentCol == 0)
                {
                    currentCol++;
                    continue;
                }
                else
                {
                    if (Mathf.Sign(currentCol) == 1)
                    {
                        newX = deployingAnchorEnemy.position.x + currentCol * 1.5f - 0.75f;
                    }
                    else
                    {
                        newX = deployingAnchorEnemy.position.x + currentCol * 1.5f + 0.75f;
                    }
                }
            }
            else
            {
                newX = deployingAnchorAlly.position.x + currentCol * 1.5f;
            }

            if (Mathf.Abs(newX) > rowSizeAlly)
            {
                currentRow++;
                currentCol = 0;
                continue;
            }
            else
            {
                float newZ = deployingAnchorAlly.position.z - currentRow * 1.5f;
                deployingPositionsAlly.Add(new Vector3(newX, 0f, newZ));

                if (currentCol > 0)
                {
                    currentCol = -currentCol;
                }
                else
                {
                    currentCol = -currentCol + 1;
                }
            }
        }
    }

    //Enemy
    private void AddListEnemyfighted(int count)
    {
        if (allyCrowd.Count > 0 && allyCrowd[0].gameObject.name.StartsWith("Main Player"))
        {

        }
        else
        {
            if (listEnemyfighted.Count > listIdx4fightingAlly.Count)
            {
                return;
            }
        }

        for (int i = 0; i < count; i++)
        {
            if (listIdx4fightingEnemy.Count > 0)
            {
                int idx = Random.Range(0, listIdx4fightingEnemy.Count);
                listEnemyfighted.Add(listIdx4fightingEnemy[idx]);
                listIdx4fightingEnemy.RemoveAt(idx);
            }
        }
    }

    public void UpdateTargetsEnemy()
    {
        if (enemyCrowd.Count > 0 && allyCrowd.Count > 0)
        {
            targetsMapEnemy.Clear();
            for (int i = 0; i < enemyCrowd.Count; i++)
            {
                targetsMapEnemy.Add(enemyCrowd[i].transform, allyCrowd[Random.Range(0, allyCrowd.Count)].transform);
            }
            foreach (var pair in targetsMapEnemy)
            {
                pair.Key.DOLookAt(pair.Value.position, Utilities.ENDGAME_AIMING_TIME);
            }
        }
    }

    public void SpawnBoss(BaseEnemyObj data)
    {
        BaseEnemyObj enemy = Instantiate(bossPrefab, deployingAnchorEnemy.position + new Vector3(0f, 0f, 10f), Quaternion.Euler(new Vector3(0f, 180f, 0f)), transform);
        enemy.copyData(data);
        enemyCrowd.Add(enemy);
    }

    private void GenDeployingPositionsEnemy()
    {
        int currentRow = 0;
        int currentCol = 0;
        deployingPositionsEnemy.Clear();

        while (deployingPositionsEnemy.Count < enemyCrowd.Count)
        {
            float newX = 0f;
            if (currentRow % 2 == 1)
            {
                if (currentCol == 0)
                {
                    currentCol++;
                    continue;
                }
                else
                {
                    if (Mathf.Sign(currentCol) == 1)
                    {
                        newX = deployingAnchorEnemy.position.x + currentCol * 1.5f - 0.75f;
                    }
                    else
                    {
                        newX = deployingAnchorEnemy.position.x + currentCol * 1.5f + 0.75f;
                    }
                }
            }
            else
            {
                newX = deployingAnchorEnemy.position.x + currentCol * 1.5f;
            }

            if (Mathf.Abs(newX) > rowSizeEnemy)
            {
                currentRow++;
                currentCol = 0;
                continue;
            }
            else
            {
                float newZ = deployingAnchorEnemy.position.z + currentRow * 1.5f;
                deployingPositionsEnemy.Add(new Vector3(newX, 0f, newZ));

                if (currentCol > 0)
                {
                    currentCol = -currentCol;
                }
                else
                {
                    currentCol = -currentCol + 1;
                }
            }
        }
    }

    private void InvadeCastle()
    {
        StartCoroutine(ShootCastleGate());
    }

    private IEnumerator ShootCastleGate()
    {
        if (PlayerPrefsUtil.Level % 3 == 1)
        {
            for (int i = 0; i < allyCrowd.Count; i++)
            {
                allyCrowd[i].GetComponent<CharacterAnimator>().OnLocomotionTrigger(0);
            }
            yield return new WaitForSeconds(3f);
        }
        else
        {
            yield return new WaitForSeconds(0.25f);
        }

        AudioManager.Instance.Play("FinishWinApplause");
        // Run toward the gate  
        cameraController.ShottingGateScene();
        for (int i = 0; i < allyCrowd.Count; i++)
        {
            allyCrowd[i].GetComponent<CharacterAnimator>().OnLocomotionTrigger(1);
            allyCrowd[i].transform.DOMoveZ(allyCrowd[i].transform.position.z + 20f, 20f / Utilities.CROWDS_STANDARD_MOVING_SPEED);
        }

        yield return new WaitForSeconds(20f / Utilities.CROWDS_STANDARD_MOVING_SPEED - 0.3f);
        StartCoroutine(AlliesDance());

        // // Shoot
        // for (int i = 0; i < allyCrowd.Count; i++)
        // {
        //     Vector3 direction = gateAnchor.position - allyCrowd[i].transform.position;
        //     allyCrowd[i].transform.DOLookAt(gateAnchor.position, Utilities.ENDGAME_AIMING_TIME);
        //     allyCrowd[i].GetComponent<CharacterAnimator>().OnShoot1Trigger(false);
        // }

        // yield return new WaitForSeconds(1f);
        // gate.SetActive(false);
        // gateExplosion.SetActive(true);
        // AudioManager.Instance.PlayOneShot("ExplosionWood", 0.7f);
        // StartCoroutine(ThrowKing());

    }

    private IEnumerator AlliesDance()
    {

        // Rotate back
        for (int i = 0; i < allyCrowd.Count; i++)
        {
            allyCrowd[i].GetComponent<CharacterAnimator>().OnLocomotionTrigger(0);
            allyCrowd[i].transform.DORotate(new Vector3(0f, 180f, 0f), Utilities.ENDGAME_AIMING_TIME);

        }

        yield return new WaitForSeconds(1f);
        // Dance   
        for (int i = 0; i < allyCrowd.Count; i++)
        {
            allyCrowd[i].GetComponent<CharacterAnimator>().OnDanceTrigger(syncDancing);
        }
        StartCoroutine(ShowWin());
    }
    IEnumerator ShowWin()
    {
        yield return new WaitForSeconds(3f);
        UIWin.SetActive(true);
    }
    private IEnumerator ThrowKing()
    {
        AudioManager.Instance.PlayOneShot("winning", 0.7f);
        //Strike
        allyCrowd[0].GetComponent<CharacterAnimator>().OnLocomotionTrigger(1);
        allyCrowd[0].transform.DOMove(gatePath[0].position, 1).OnKill(() => allyCrowd[0].transform.DOMove(gatePath[1].position, 1)).SetEase(Ease.Linear).OnStart(() => allyCrowd[0].transform.DOLookAt(gatePath[1].position, 2));
        yield return new WaitForSeconds(2f);
        cameraController.RecordWinningScene();
        AudioManager.Instance.Play("FireworksApplause", true);
        winningEffect.SetActive(true);
        // Throw king
        allyCrowd[0].transform.position = kingSpawnAnchor.position;
        allyCrowd[0].transform.DORotate(new Vector3(0f, 180f, 0f), 0.1f);
        allyCrowd[0].transform.DOMove(heroDanceAnchor.position, 0.75f);
        allyCrowd[0].GetComponent<CharacterAnimator>().OnDanceTrigger(syncDancing);
        yield return new WaitForSeconds(0.25f);
        if (mygame.sdk.PlayerPrefsUtil.Level < 10)
        {
            yield return new WaitForSeconds(5f);
        }
        else
        {
            yield return new WaitForSeconds(3f);
        }


    }



    private IEnumerator EnemiesDance()
    {
        for (int i = 0; i < enemyCrowd.Count; i++)
        {
            enemyCrowd[i].transform.DORotate(new Vector3(0f, 180f, 0f), Utilities.ENDGAME_AIMING_TIME / 3f);
        }

        yield return new WaitForSeconds(Utilities.ENDGAME_AIMING_TIME / 3f);

        // Dance
        for (int i = 0; i < enemyCrowd.Count; i++)
        {
            enemyCrowd[i].GetComponent<CharacterAnimator>().OnDanceTrigger(syncDancing);
        }

        // Show losing UI
        yield return new WaitForSeconds(5.5f);
    }
}
