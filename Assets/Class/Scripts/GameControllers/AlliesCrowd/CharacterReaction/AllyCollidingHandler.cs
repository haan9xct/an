﻿using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;
using mygame.sdk;
using UnityEngine.SceneManagement;
public class AllyCollidingHandler : MonoBehaviour
{
    [Header("References")]
    private AlliesController alliesController;
    private AllyStatus selfStatus;
    [SerializeField] Collider myCollider;
    [SerializeField] Rigidbody rigidBody;
    [Header("Obstacles Destruction FX Prefab")]
    public Transform destructionFXPrefab;
    public GameMaster gameMaster;

    [Header("FX")]
    [SerializeField] GameObject bloodFx;
    private BloodFXDelayer bloodFXDelayer;
    List<Vector3> tempoFx = new List<Vector3>();

    private void Awake()
    {
        if (GameMaster.Instance != null)
        {
            bloodFXDelayer = GameMaster.Instance.GetComponent<BloodFXDelayer>();
        }
        selfStatus = transform.parent.GetComponent<AllyStatus>();
    }

    private void Start()
    {
        gameMaster = GameObject.FindWithTag("GameController").GetComponent<GameMaster>();
        bloodFXDelayer = GameMaster.Instance.GetComponent<BloodFXDelayer>();
    }
    public void ResetObj()
    {
        transform.localPosition = Vector3.zero;
        myCollider.enabled = true;
        rigidBody.isKinematic = true;
        gameObject.layer = LayerMask.NameToLayer("Neutral Character");
    }
    // Needed to check colliding of fragile obstacle exclusively cause its collider will be disabled 1 frame after touching
    // OnTriggerStay can't be run
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("collision obstance");

        if (other.transform.CompareTag(Utilities.FRAGILE_OBSTACLE_STANDARD_COLLIDER))
        {
            // Die on touching colliders of fragile obstacle
            GetComponent<CapsuleCollider>().enabled = false;

            // Die
            alliesController = GetComponentInParent<AlliesController>();
            alliesController.KillAnAlly(transform.parent);
            AudioManager.Instance.PlayOneShot("Death", 0.7f);
            selfStatus.isDeath = true;
            // Play death fx
            GetComponentInParent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Allied,true);
            this.enabled = false;
            Debug.Log(" obstance");

        }

        // NOTE: Blood fx off temporarily
        if (other.gameObject.layer == LayerMask.NameToLayer("EnemyBullet"))
        {
            // Delay playing blood fx
            if (bloodFXDelayer.IncreasePlayingAllyBloodFx())
            {
                Transform blood = PoolManager.Pools["Game"].Spawn(bloodFx, transform.position + Vector3.up * 1.5f, Quaternion.identity);
                blood.GetComponent<BloodFX>().SetColor(ThemesController.Instance.allyBloodFXMaterials[ThemesController.Instance.currentIndexAllyMat], ThemesController.Instance.allyBloodFXMaterials[ThemesController.Instance.currentIndexAllyMat].color);
                PoolManager.Pools["Game"].Despawn(blood, 2f);
            }
            if (other.GetComponent<BulletController>() != null)
            {

                PoolManager.Pools["Game"].Despawn(other.transform);
            }

        }
        CheckColl( other);
    }

  


    private IEnumerator StartBloodFxCooldown()
    {
        yield return new WaitForSeconds(1.6f);
        //bloodFxPlaying = false;
    }


    private void CheckColl(Collider other)
    {
        if (other.transform.CompareTag(Utilities.OBSTACLE_STANDARD_COLLIDER_TAG))
        {
            Debug.Log("collision obstance");

            if (selfStatus.invulnerable)
            {
                // if (transform.parent.parent.GetComponent<AlliesMovement>() != null && transform.parent.parent.GetComponent<AlliesMovement>().Boosting)
                // {
                //     if (other.transform.parent.CompareTag(Utilities.DESTROYABLE_OBSTACLE_TAG))
                //     {
                //         tempoFx.Clear();

                //         AudioManager.Instance.PlayOneShot("Explosion", 0.5f);
                //         Destroy(other.transform.parent.gameObject);
                //         foreach (var pos in tempoFx)
                //         {
                //             Transform fx = Instantiate(destructionFXPrefab, pos, Quaternion.identity);
                //             Destroy(fx.gameObject, 4f);
                //         }
                //     }
                // }
                // return;
            }

            GetComponent<CapsuleCollider>().enabled = false;
            selfStatus.isDeath = true;
            if (GetComponentInParent<AlliesController>() != null)
            {
                alliesController = GetComponentInParent<AlliesController>();
                alliesController.KillAnAlly(transform.parent);
                AudioManager.Instance.PlayOneShot("Death", 0.7f);

                GetComponentInParent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Allied, true);
            }
            else
            {
                GetComponentInParent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Neutral, true);
            }
            transform.parent.GetChild(2).gameObject.SetActive(false);

            this.enabled = false;
            gameMaster.EndGame();
        }
    }
}
