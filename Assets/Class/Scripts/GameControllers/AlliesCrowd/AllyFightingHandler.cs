﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;

public class AllyFightingHandler : MonoBehaviour
{
    [Header("References")]
    private AlliesController alliesController;

    [Header("Fighting parameters")]
    private Queue<Transform> triggeredAllies = new Queue<Transform>();
    private Queue<Transform> triggeredEnemies = new Queue<Transform>();


    private void Start()
    {
        alliesController = gameObject.GetComponent<AlliesController>();
    }


    public void OnGunFightDetected(Transform opponent, Transform triggeredAlly)
    {
        triggeredAllies.Enqueue(triggeredAlly);
        triggeredEnemies.Enqueue(opponent);
        for (int i = 0; i < triggeredAlly.childCount; i++)
        {
            if (triggeredAlly.GetChild(i).CompareTag(Utilities.NEUTRAL_STANDARD_COLLIDER_TAG))
            {
                triggeredAlly.GetChild(i).gameObject.SetActive(false);
                break;
            }
        }
        alliesController.KillAnAlly(triggeredAlly);
        float movingTime = Vector3.Distance(opponent.position + Vector3.back * 10f, triggeredAlly.position) / (Utilities.CROWDS_STANDARD_MOVING_SPEED * 1.8f);
        triggeredAlly.DOMove(opponent.position + Vector3.back * 10f, movingTime).OnStart(() => triggeredAlly.DOLookAt(opponent.position, movingTime - 0.1f)).OnComplete(() =>
        {
            triggeredAlly.LookAt(opponent);
            PlayFightingAnimation();
        });

    }


    private void PlayFightingAnimation()
    {
        Transform ally = triggeredAllies.Dequeue();
        Transform enemy = triggeredEnemies.Dequeue();

        ally.GetComponent<CharacterAnimator>().OnShoot1Trigger(false);
        enemy.GetComponent<CharacterAnimator>().OnShoot1Trigger(false);
        StartCoroutine(Die(ally, enemy));
    }


    IEnumerator Die(Transform ally, Transform enemy)
    {
        yield return new WaitForSeconds(1f);
        AudioManager.Instance.PlayOneShot("Death", 0.7f);
        ally.GetComponent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Allied,true);
        enemy.GetComponent<CharactersDeathFX>().DisplayDeathFX(CharactersDeathFX.CharType.Enemy,true);
    }
}
