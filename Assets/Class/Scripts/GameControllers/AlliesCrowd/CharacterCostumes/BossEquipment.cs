﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;


public class BossEquipment : MonoBehaviour
{
    [Header("Private status parameters")]
    private WeaponInfo currentGun;
    private int currentGunIndex = 0;
    [SerializeField] Transform weaponHolder1;
    [SerializeField] Transform weaponHolder2;
    public List<WeaponInfo> listWeapon = new List<WeaponInfo>();
    private void Awake()
    {
        // Check and reuse the last boss gun if not playing for the first time
        int memidxgun = PlayerPrefsUtil.memGunBoss;
        int randGun;
        if (memidxgun < 0)
        {
            //Guns ranking
            if (PlayerPrefsUtil.Level > 18)
            {
                randGun = Random.Range(2, listWeapon.Count);
                PlayerPrefsUtil.memGunBoss = randGun;
            }
            else
            {
                randGun = (PlayerPrefsUtil.Level / 3) - 1;
                PlayerPrefsUtil.memGunBoss = randGun;
            }
            currentGunIndex = randGun;
        }
        else
        {
            currentGunIndex = memidxgun;
        }
       // UpdateGun();
    }


    public void UpdateGun()
    {
        currentGun = Instantiate(listWeapon[0], weaponHolder1);
        // Enable the second gun if using dual pistol
        if (currentGun.is2Handed)
        {
            weaponHolder2.gameObject.SetActive(true);
            Instantiate(listWeapon[0], weaponHolder2);
        }
    
        // Update gun animations
        //GetComponent<BossAnimator>().UpdateGun(currentGun);
    }
}
