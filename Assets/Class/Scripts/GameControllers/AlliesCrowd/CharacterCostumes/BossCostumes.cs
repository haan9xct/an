﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;

public class BossCostumes : MonoBehaviour
{
    [Header("References")]
    public Transform headAnchor;
    private int costumesCount = 8;

	void Start()
	{
		int lvl = PlayerPrefsUtil.Level;
		int randCostume;

		// Pick the right costume
		if (lvl > 24)
		{
			randCostume = Random.Range(0, costumesCount);
		}
		else
		{
			randCostume = (lvl / 3) - 1;
		}

		for (int i = 0; i < costumesCount; i++)
		{
			if (i == randCostume)
			{
				headAnchor.GetChild(i).gameObject.SetActive(true);
			}
			else
			{
				headAnchor.GetChild(i).gameObject.SetActive(false);
			}
		}
	}
}
