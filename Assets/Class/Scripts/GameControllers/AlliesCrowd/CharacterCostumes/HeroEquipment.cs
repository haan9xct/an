﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;

public class HeroEquipment : MonoBehaviour
{
    [SerializeField] Transform gunHolder;
    [SerializeField] HeroAnimator heroAnimator;
    public WeaponInfo currentWeapon = null;
    public HeadHolderSlot headEquip;

    private void Start()
    {
        EquipWeapon(PlayerPrefsUtil.CurrentWeaponItem);
        headEquip.EquipItem(PlayerPrefsUtil.CurrentItemOutfit);
    }

    public void EquipWeapon(string _name)
    {
        if (currentWeapon != null)
        {
            Destroy(currentWeapon.gameObject);
        }

        if (Resources.Load<WeaponInfo>("Weapon/" + _name))
        {
            currentWeapon = Instantiate(Resources.Load<WeaponInfo>("Weapon/" + _name), gunHolder);
        }

        PlayerPrefsUtil.CurrentWeaponItem = _name;
        heroAnimator.UpdateCurrentGun(currentWeapon.animationName);
    }
}
