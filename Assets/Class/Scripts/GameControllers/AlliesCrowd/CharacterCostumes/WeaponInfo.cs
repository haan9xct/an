﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteAlways]
public class WeaponInfo : MonoBehaviour
{
    public int damage = 0;
    public int armor = 0;
    public string weaponName;
    public string animationName;
    public bool is2Handed;
    public MoveType bulletMoveType;
    public AudioClip[] soundFX;
    public GameObject animution;
    public Transform firePoint;
    public ParticleSystem muzzleFX;
    
    private void Start()
    {
        weaponName = gameObject.name;
    }
}
