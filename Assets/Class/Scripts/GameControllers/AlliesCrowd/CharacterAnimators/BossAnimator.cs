﻿using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class BossAnimator : CharacterAnimator
{

    [HideInInspector] public bool isFire = false;
    [SerializeField] GameObject fire;
    [SerializeField] GameObject prefabExplodion;

    [SerializeField] Transform posAttack;
    GameObject fireAttack;


    [Header("Private status parameters")]
    private WeaponInfo currentGun;

    AnimationStates currentState;
    public void Attack()
    {
        if (!isFire)
        {
            isFire = true;
          fireAttack =  Instantiate(fire, posAttack.position, new Quaternion(0, 180, 0, 0));
        }
    }
    public void Dead()
    {
        Destroy(fireAttack);
        StartCoroutine(FxExplosion(0.5f));
    }

    IEnumerator FxExplosion(float dt)
    {
        yield return new WaitForSeconds(dt);
        Debug.Log("vao explosion");
       Instantiate(prefabExplodion,transform.position,Quaternion.identity);
       transform.rotation = Quaternion.Euler(-90, 0, 0);
       PoolManager.Pools["Game"].Spawn("FXWinning", transform.position, Quaternion.identity);

    }
    public void UpdateGun(WeaponInfo weaponInfo)
    {
        currentGun = weaponInfo;
        OnLocomotionTrigger(0);
    }


    public override void OnDanceTrigger(bool instantAct)
    {
        if (currentState != AnimationStates.Dancing)
        {
            TriggerAnimation(currentGun.animationName + Utilities.DANCE_ANIMATION_NAME, 0f);
            currentState = AnimationStates.Dancing;
        }
    }


    public override void OnLocomotionTrigger(float speed)
    {
        selfAnimator.SetFloat("speed", speed);
        if (currentGun != null)
        {
            TriggerAnimation(currentGun.animationName + Utilities.IDLE_ANIMATION_NAME, 0f);
        }
        currentState = AnimationStates.Idle;
    }





    public override void OnShoot1Trigger(bool instantAct)
    {

        // Shoot normally with the rest gun types
        if (currentState != AnimationStates.Shooting_1)
        {
            TriggerAnimation(currentGun.animationName + Utilities.SHOOT1_ANIMATION_NAME, 0f);
            currentState = AnimationStates.Shooting_1;
        }
    }




    public void OnDeadTrigger()
    {
        currentState = AnimationStates.Idle;

        TriggerAnimation("Boss_Dead", 0f);
    }

    protected override void TriggerAnimation(string animationName, float maxOffset)
    {
        {
            // Base layer = -1
            selfAnimator.Play(animationName);
        }
    }

    public override void ResetObj()
    {

    }

    public override void UpdateGun()
    {
    }
}
