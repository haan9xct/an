﻿using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class BossAnimationEvent : MonoBehaviour
{
    [SerializeField] Transform weaponHolder1;
    [SerializeField] Transform weaponHolder2;
    public Transform deathFXPrefab;
    public float fxScale = 1f;
    public GameObject prefabFX;

    void PlayDualGunFX(int indexHand)
    {
        if (indexHand == 1)
        {
            WeaponInfo weapon = weaponHolder1.GetComponentInChildren<WeaponInfo>();
           // Debug.Log(weapon.gameObject.name);
            SpawnFX(weapon, weapon.firePoint.position, weapon.firePoint.right);
        }
        else
        {
            WeaponInfo weapon = weaponHolder2.GetComponentInChildren<WeaponInfo>();
            SpawnFX(weapon, weapon.firePoint.position, weapon.firePoint.right);
        }
    }

    void SpawnBullet(WeaponInfo weapon, Vector3 spawnPos, Vector3 direction)
    {
        weapon.muzzleFX.Play();
        AudioManager.Instance.PlayOneShotByClip(weapon.soundFX[Random.Range(0, weapon.soundFX.Length)], 0.6f);
        Transform bullet = PoolManager.Pools["Game"].Spawn(weapon.animution, spawnPos, Quaternion.identity);
        bullet.GetComponent<BulletController>().Move(direction, 50, weapon.bulletMoveType, LayerMask.NameToLayer("EnemyBullet"));
        PoolManager.Pools["Game"].Despawn(bullet, 3f);
    }
    void SpawnFX(WeaponInfo weapon, Vector3 spawnPos, Vector3 direction)
    {
        weapon.muzzleFX.Play();
        AudioManager.Instance.PlayOneShotByClip(weapon.soundFX[Random.Range(0, weapon.soundFX.Length)], 0.6f);
        Transform bullet = PoolManager.Pools["Game"].Spawn(prefabFX, spawnPos, new Quaternion(0,180,0,0));
       // bullet.GetComponent<BulletController>().Move(direction, 50, weapon.bulletMoveType, LayerMask.NameToLayer("EnemyBullet"));
        //PoolManager.Pools["Game"].Despawn(bullet, 3f);
        PoolManager.Pools["Game"].Despawn(bullet, 3f);

    }
    void BossDeadFX()
    {
        Transform fx = PoolManager.Pools["Game"].Spawn(deathFXPrefab, transform.position, Quaternion.identity);
        fx.localScale = Vector3.one * fxScale;
    }
}

