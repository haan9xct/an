﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum AnimationStates
{
	None,
	Idle,
	Running_Straight,
	Running_And_Shooting_Straight,
	ShowingOff,
	Flipping,
	Dancing,
	Shooting_1,
	Shooting_2
}

public enum AnimationSpeedMultiplier
{
	Running_Straight_Multiplier
}


public abstract class CharacterAnimator : MonoBehaviour
{
	public Animator selfAnimator;
	public abstract void UpdateGun();
	public abstract void OnDanceTrigger(bool instantAct);
	public abstract void OnLocomotionTrigger(float speed);
	public abstract void OnShoot1Trigger(bool instantAct);
	protected abstract void TriggerAnimation(string animationName, float maxOffset);

	public abstract void ResetObj();
	public virtual void OnFlipTrigger(bool instantAct) { }
	public virtual void OnRunAndShootStraightTrigger(bool instantAct) { }
	public virtual void ChangeAnimationSpeedMultiplier(AnimationSpeedMultiplier name, float multiplier) { }
}
