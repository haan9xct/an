﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAnimator : CharacterAnimator
{

    [Header("References")]
    [SerializeField]
    private AllyStatus allyStatus;

    [Header("Status parameters")]
    private AnimationStates currentState = AnimationStates.None;

    [Header("Private status parameters")]
    private string currentGun;


    private void Start()
    {
        allyStatus = GetComponent<AllyStatus>();
    }


    public void ShowOffWeapon()
    {
        if (currentState != AnimationStates.ShowingOff)
        {
            TriggerAnimation(currentGun + Utilities.SHOW_OFF_ANIMATION_NAME, 0f);
            currentState = AnimationStates.ShowingOff;
        }
        currentState = AnimationStates.Idle;
    }


    public void UpdateCurrentGun(string _gunName)
    {
        if (selfAnimator == null)
        {
            selfAnimator = transform.GetChild(0).GetComponent<Animator>();
        }

        if (allyStatus == null)
        {
            allyStatus = GetComponent<AllyStatus>();

        }

        currentGun = _gunName;

        OnLocomotionTrigger(0);
    }

    public void UpdateAndShowOffGun(string _gunName)
    {
        if (selfAnimator == null)
        {
            selfAnimator = transform.GetChild(0).GetComponent<Animator>();
        }

        if (allyStatus == null)
        {
            allyStatus = GetComponent<AllyStatus>();

        }

        currentGun = _gunName;

        ShowOffWeapon();
    }

    public override void OnDanceTrigger(bool sync)
    {
        if (currentState != AnimationStates.Dancing)
        {
            TriggerAnimation(currentGun + Utilities.DANCE_ANIMATION_NAME, 0f);
            currentState = AnimationStates.Dancing;
        }
    }


    public override void OnFlipTrigger(bool instantAct)
    {
        if (currentState != AnimationStates.Flipping)
        {
            TriggerAnimation(currentGun + Utilities.FLIP_ANIMATION_NAME, 0f);
            currentState = AnimationStates.Flipping;
        }
    }


    public override void OnLocomotionTrigger(float speed)
    {
       

		selfAnimator.SetFloat("speed",speed);
		Debug.Log("speed move ="+speed);

        TriggerAnimation(currentGun + Utilities.IDLE_ANIMATION_NAME, 0f);

        currentState = AnimationStates.Idle;
    }

    public override void OnShoot1Trigger(bool instantAct)
    {
        if (currentState != AnimationStates.Shooting_1)
        {
            TriggerAnimation(currentGun + Utilities.SHOOT1_ANIMATION_NAME, 0f);
            currentState = AnimationStates.Shooting_1;
        }
    }
    protected override void TriggerAnimation(string animationName, float maxOffset)
    {
        {
            // Base layer = -1
            selfAnimator.Play(animationName);
            Debug.Log("animation ="+animationName);
        }
    }


    public override void ChangeAnimationSpeedMultiplier(AnimationSpeedMultiplier name, float multiplier)
    {
        selfAnimator.SetFloat(name.ToString(), multiplier);
    }

    public override void UpdateGun()
    {

    }

    public override void ResetObj()
    {

    }
}
