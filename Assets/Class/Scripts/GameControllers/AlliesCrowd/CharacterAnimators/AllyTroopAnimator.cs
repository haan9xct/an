﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyTroopAnimator : CharacterAnimator
{
    [Header("References")]
    private AllyStatus allyStatus;

    [Header("Status parameters")]
    private AnimationStates currentState = AnimationStates.None;


    private void Start()
    {
        allyStatus = GetComponent<AllyStatus>();
        OnLocomotionTrigger(0);
    }


    public override void UpdateGun()
    {
        OnLocomotionTrigger(0);
    }


    public override void OnDanceTrigger(bool instantAct)
    {
        if (currentState != AnimationStates.Dancing)
        {
            if (instantAct)
            {
                TriggerAnimation(Utilities.DANCE_ANIMATION_NAME, 0f);
            }
            else
            {
                TriggerAnimation(Utilities.DANCE_ANIMATION_NAME, 0.5f);
            }

            currentState = AnimationStates.Dancing;
        }
    }

    public override void OnLocomotionTrigger(float speed)
    {
        // Doesn't idle when jumping
      
        TriggerAnimation(Utilities.IDLE_ANIMATION_NAME, 0f);
        selfAnimator.SetFloat("speed", speed);
        Debug.Log("speed="+speed);
        currentState = AnimationStates.Idle;
    }

    public override void OnShoot1Trigger(bool instantAct)
    {
        if (currentState != AnimationStates.Shooting_1)
        {
            if (instantAct)
            {
                TriggerAnimation(Utilities.SHOOT1_ANIMATION_NAME, 0f);

            }
            else
            {
                TriggerAnimation(Utilities.SHOOT1_ANIMATION_NAME, 0.7f);
            }
            currentState = AnimationStates.Shooting_1;
        }
    }
    protected override void TriggerAnimation(string animationName, float maxOffset)
    {
        {
            // Base layer = -1
            selfAnimator.Play(animationName);
            Debug.Log("triger="+animationName);
        }
    }


    public override void ChangeAnimationSpeedMultiplier(AnimationSpeedMultiplier name, float multiplier)
    {
        Debug.Log("name ="+name);
        selfAnimator.SetFloat(name.ToString(), multiplier);
    }

    public override void ResetObj()
    {
        selfAnimator.SetFloat("speed", 0);
    }
}
