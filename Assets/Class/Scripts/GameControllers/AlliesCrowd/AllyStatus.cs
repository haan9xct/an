﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyStatus : MonoBehaviour
{
    public bool invulnerable = false;
    public bool Invulnerable { get => invulnerable; set => invulnerable = value; }
    public bool isDeath { get; set; }
    public void ResetObj()
    {
        invulnerable = false;
        isDeath = false;
    }
}
