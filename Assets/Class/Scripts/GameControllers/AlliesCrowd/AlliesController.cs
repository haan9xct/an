using PathologicalGames;
using System.Collections.Generic;
// using System.Collections.Specialized;
using UnityEngine;

public class AlliesController : MonoBehaviour
{
    [Header("Refences")]
    public GameMaster gameMaster;
    public Transform deadCharsContainer;

    [Header("Prefabs")]
    public Transform heroPrefab;

    [Header("Crowd members")]
    private List<Transform> alliesCrowd = new List<Transform>();

    [Header("Crowd anchors")]
    private Transform leftChar;
    private Transform rightChar;
    private Vector3 bottomAnchor;
    private Transform centerChar;
    private Transform topChar;
    public Transform mainCharacter;

    [Header("Appearance parameters")]
    public Material allyMaterial;

    public Material mainAllyMaterial;
    #region Getters & Setters
    public List<Transform> AlliesCrowd { get => alliesCrowd; }
    public Transform LeftChar { get => leftChar; }
    public Transform RightChar { get => rightChar; }
    public Transform CenterChar { get => centerChar; }
    public Transform TopChar { get => topChar; }
    #endregion

    private void Start()
    {
        Reset();
    }
    public void Reset()
    {
        // Remove old data
        int n = transform.childCount - 1;
        for (int i = n; i >= 0; i--)
        {
            Transform tf = transform.GetChild(i);
            tf.parent = gameMaster.transform;
            Destroy(tf.gameObject);
        }
        alliesCrowd.Clear();
        GetComponent<AlliesMovement>().OnReset();
        // Respawn hero and update
        var allyMain = Instantiate(heroPrefab, Vector3.zero, Quaternion.identity, transform);
        mainCharacter = allyMain;
        alliesCrowd.Add(allyMain);
        UpdateCrowdAnchors();
        UpdateCrowdCommander();
    }


    public void UpdateAllCrowdData()
    {
        UpdateCrowdMembers();
        UpdateCrowdAnchors();
        UpdateCrowdCommander();
    }


    public void OnCrowdGrowth()
    {
        UpdateCrowdMembers();
        UpdateCrowdAnchors();
    }


    public void KillAnAlly(Transform deadAlly)
    {
        deadAlly.parent = deadCharsContainer;

        UpdateCrowdMembers();

        if (alliesCrowd.Count >= 1)
        {
            UpdateCrowdAnchors();
            // Update center character if the ex is dead
            if (!alliesCrowd.Contains(centerChar))
            {
                UpdateCrowdCommander();
            }
        }
       
    }


    private void UpdateCrowdMembers()
    {
        // bool invulnerable = GetComponent<AlliesMovement>().Boosting;
        alliesCrowd.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            AllyStatus status = child.GetComponent<AllyStatus>();
            if (status != null && status.isDeath == false)
            {
                alliesCrowd.Add(child);
                SkinnedMeshRenderer memMeshRenderer = child.GetComponentInChildren<SkinnedMeshRenderer>();
                if (memMeshRenderer.material.color != allyMaterial.color)
                {
                    memMeshRenderer.material = allyMaterial;
                }
                if (child.name.Contains("Main"))
                {
                    memMeshRenderer.material = mainAllyMaterial;
                }
                // if (invulnerable)
                // {
                //     child.GetComponent<AllyStatus>().Invulnerable = true;
                // }
            }
        }
    }
    private void UpdateCrowdAnchors()
    {
        if (alliesCrowd.Count == 1)
        {
            leftChar = rightChar = topChar = alliesCrowd[0].transform;
        }
        else if (alliesCrowd.Count > 1)
        {
            Transform leftAnchor, rightAnchor, topAnchor, bottomAnchor;
            leftAnchor = rightAnchor = topAnchor = bottomAnchor = alliesCrowd[0].transform;

            for (int i = 1; i < alliesCrowd.Count; i++)
            {
                Vector3 checkingPos = alliesCrowd[i].transform.position;
                if (checkingPos.x < leftAnchor.position.x)
                {
                    leftAnchor = alliesCrowd[i].transform;
                }
                if (checkingPos.x > rightAnchor.position.x)
                {
                    rightAnchor = alliesCrowd[i].transform;
                }
                if (checkingPos.z > topAnchor.position.z)
                {
                    topAnchor = alliesCrowd[i].transform;
                }
                if (checkingPos.z < bottomAnchor.position.z)
                {
                    bottomAnchor = alliesCrowd[i].transform;
                }
            }
            leftChar = leftAnchor;
            rightChar = rightAnchor;
            topChar = topAnchor;
            this.bottomAnchor = bottomAnchor.position;
        }
    }
    private void UpdateCrowdCommander()
    {
        if (alliesCrowd.Count == 1)
        {
            centerChar = alliesCrowd[0].transform;
        }
        float xOffset = (rightChar.position.x - leftChar.position.x) / 2;
        float zOffset = (topChar.position.z - bottomAnchor.z) / 2;
        Vector3 centerPoint = new Vector3(leftChar.position.x + xOffset, leftChar.position.y, bottomAnchor.z + zOffset);

        Transform centerAnchor = alliesCrowd[0].transform;
        for (int i = 1; i < alliesCrowd.Count; i++)
        {
            if (Vector3.Distance(alliesCrowd[i].transform.position, centerPoint) < Vector3.Distance(centerAnchor.position, centerPoint))
            {
                centerAnchor = alliesCrowd[i].transform;
            }
        }
        centerChar = centerAnchor;
    }


    public void UpdateCrowdImmortalityStatus(bool invulnerable)
    {
        foreach (var ally in alliesCrowd)
        {
            ally.GetComponent<AllyStatus>().Invulnerable = invulnerable;
        }
    }
}


