﻿using UnityEngine;

public class AllyGeneral : MonoBehaviour
{
    [SerializeField] AllyCollidingHandler allyCollidingHandler;
    [SerializeField] AlliesGatherer alliesGatherer;
    [SerializeField] AllyStatus allyStatus;
    [SerializeField] BaseEnemyObj baseEnemyObj;
    [SerializeField] CharacterAnimator allyTroopAnimator;
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] Material neutralMaterial;
    public void ResetCharacter()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        if (meshRenderer != null)
        {
            meshRenderer.material = neutralMaterial;
        }
        transform.position = Vector3.zero;
        allyCollidingHandler?.ResetObj();
        alliesGatherer?.ResetObj();
        allyStatus?.ResetObj();
        baseEnemyObj?.ResetObj();
        allyTroopAnimator?.ResetObj();
    }
}
