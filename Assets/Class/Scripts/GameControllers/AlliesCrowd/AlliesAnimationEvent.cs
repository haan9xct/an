﻿using PathologicalGames;
using UnityEngine;

public class AlliesAnimationEvent : MonoBehaviour
{
    [SerializeField] GameObject gunHolder;
    [SerializeField] Transform firePoint;
    void PlayGunFX()
    {

        WeaponInfo weaponInfo = gunHolder.GetComponentInChildren<WeaponInfo>();
        SpawnBullet(weaponInfo, weaponInfo.firePoint.position, weaponInfo.firePoint.right, 0.7f);
    }

    void PlayPistolFX()
    {
        WeaponInfo weaponInfo = gunHolder.GetComponentInChildren<WeaponInfo>();
        SpawnBullet(weaponInfo, firePoint.position, firePoint.forward, 0.4f);
    }

    void SpawnBullet(WeaponInfo weapon, Vector3 spawnPos, Vector3 direction, float audioVolume)
    {
        AudioManager.Instance.PlayOneShot("RifleShoot", 1f);

        weapon.muzzleFX.Play();
        AudioManager.Instance.PlayOneShotByClip(weapon.soundFX[Random.Range(0, weapon.soundFX.Length)], audioVolume);
        Transform bullet = PoolManager.Pools["Game"].Spawn(weapon.animution, spawnPos, Quaternion.identity);
        bullet.GetComponent<BulletController>().Move(direction, 50, weapon.bulletMoveType, LayerMask.NameToLayer("AllyBullet"));
        PoolManager.Pools["Game"].Despawn(bullet, 3f);
    }
}
