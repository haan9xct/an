﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using mygame.sdk;

public class AlliesMovement : MonoBehaviour
{
    [Header("References")]
    public GameMaster gameMaster;
    public EndgameController endgameController;
    private AlliesController alliesController;
    private float speedmoveHozi = 0;

    private float maxSpeedHori = 12.5f;

    private float kIncreaseSpeedHori = 1.0f;

    private float kConvertPxtoWorldUnit = 45.0f;

    private float kModeSpeed = 1.5f;

    private float kcheckzero = 0.005f;

    private float kcheckpx = 2.0f;

    private float kSpeedCurr = 1;

    private float XAxisInput;

    private float prePos = -10000;

    private Vector3 currRotaion = Vector3.zero;

    private float CurrRotationY = 0;
    public Slider slHspeed;
    public Slider slds;
    bool isPlayRunningSound = false;
    public GameObject speedWarpFX;

    private int count4DesreaseRot = 0;
    float moveAmount;

    private void Start()
    {
        alliesController = GetComponent<AlliesController>();
        endgameController = gameMaster.GetComponent<EndgameController>();
        kConvertPxtoWorldUnit = mygame.sdk.PlayerPrefsUtil.sensitivity;
        minsh = mygame.sdk.PlayerPrefsUtil.minVelocityHoz;
        maxsh = mygame.sdk.PlayerPrefsUtil.maxVelocityHoz;
        if (minsh >= maxsh)
        {
            maxsh = minsh + 1;
        }
        //test
        if (slHspeed != null)
        {
            slHspeed.transform.Find("Text").GetComponent<Text>().text = ("" + slHspeed.value);
            slHspeed.onValueChanged.AddListener(x =>
            {
                maxSpeedHori = x;
                slHspeed.transform.Find("Text").GetComponent<Text>().text = ("" + x);
            });
            slds.transform.Find("Text").GetComponent<Text>().text = ("" + slds.value);
            slds.onValueChanged.AddListener(x =>
            {
                kIncreaseSpeedHori = x;
                slds.transform.Find("Text").GetComponent<Text>().text = ("" + x);
            });
        }
    }


    public void StartBoosting()
    {
        speedWarpFX.SetActive(true);
        StartCoroutine(DisableSpeedFX());
        alliesController.UpdateCrowdImmortalityStatus(true);

        kSpeedCurr = kModeSpeed;

    }

    IEnumerator DisableSpeedFX()
    {
        yield return new WaitForSeconds(Utilities.BOOSTING_DURATION);
        speedWarpFX.SetActive(false);
    }

    public void OnReset()
    {
        moveAmount = 0;
    }

    private void Update()
    {
        if (GameMaster.gameStates == GameStates.ENDGAME && GameMaster.endgameStates == EndgameStates.PRE_BATTLE)
        {
            MoveVertically();
            DecreaseRotation();
            gameMaster.OnAlliesMoving();
            return;
        }

        if (GameMaster.gameStates == GameStates.ENDGAME || GameMaster.gameStates == GameStates.MENU) return;

        // Move freely 
        if (GameMaster.gameStates == GameStates.INGAME)
        {
            if (Input.GetMouseButtonDown(0))
            {
                AudioManager.Instance.Play("Running", true);
                prePos = Input.mousePosition.x;
                XAxisInput = 0;
                speedmoveHozi = 0;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                AudioManager.Instance.Stop("Running");
                isPlayRunningSound = false;
                XAxisInput = 0;
                speedmoveHozi = 0;
                prePos = Input.mousePosition.x;
            }

            if (!Input.GetMouseButton(0))
            {
                AudioManager.Instance.Stop("Running");
                DecreaseRotation();
            }


            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                MoveVertically();
                // Turn direction
                if (XAxisInput < -kcheckzero && (Input.mousePosition.x - prePos) > kcheckpx)
                {
                    XAxisInput = 0;
                    currRotaion.y = -2;
                    //speedmoveHozi = 0;
                }
                else if (XAxisInput > kcheckzero && (Input.mousePosition.x - prePos) < -kcheckpx)
                {
                    XAxisInput = 0;
                    //speedmoveHozi = 0;
                    currRotaion.y = 2;
                }

                XAxisInput += (Input.mousePosition.x - prePos) / kConvertPxtoWorldUnit;
                prePos = Input.mousePosition.x;

                if (XAxisInput > kcheckzero || XAxisInput < -kcheckzero)
                {
                    MoveHorizontally();
                }
                else
                {
                    count4DesreaseRot++;
                    if (count4DesreaseRot >= 1)
                    {
                        DecreaseRotation();
                    }
                }
                moveAmount = 1;
                // Notify game master to move camera, landscape background
                gameMaster.OnAlliesMoving();
            }
            else
            {
                moveAmount -= 0.1f;
                if (moveAmount <= 0) moveAmount = 0;
                if (moveAmount > 0)
                {
                    for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
                    {
                        Transform character = alliesController.AlliesCrowd[i].transform;
                        character.position = new Vector3(character.position.x, character.position.y, character.position.z + 2 * Time.deltaTime);
                    }
                    gameMaster.OnAlliesMoving();
                }
                DecreaseRotation();
            }
        }
        for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
        {
            alliesController.AlliesCrowd[i].GetComponent<CharacterAnimator>().selfAnimator.SetFloat("speed", moveAmount);
        }

    }

    float dxxxx = 0;


    private void MoveVertically()
    {

        for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
        {
            Transform character = alliesController.AlliesCrowd[i].transform;
            float newZ = character.position.z + Utilities.CROWDS_STANDARD_MOVING_SPEED * Time.deltaTime;
            character.position = new Vector3(character.position.x, character.position.y, newZ);
            {
                CharacterAnimator characterAnimator = character.GetComponent<CharacterAnimator>();
            }
        }
    }


    private void MoveHorizontally()
    {
        float dx = 0;
        int type = -1 ;
        if (XAxisInput < -kcheckzero)
        {
            type = 0;
            count4DesreaseRot = 0;
            IncreaseSpeed();

            if (PlayerPrefsUtil.IsMoveFollowATouch == 1)
            {
                CalculateRotation();
                float maxv1 = PlayerPrefsUtil.maxMoveWhen1;
                if (XAxisInput < -maxv1)
                {
                    XAxisInput -= -maxv1;
                    dx = -maxv1;
                }
                else
                {
                    dx = XAxisInput;
                    XAxisInput = 0;
                }
            }
            else
            {
                dx = -speedmoveHozi * Time.deltaTime;
                CalculateRotation();
                if (XAxisInput < dx)
                {
                    XAxisInput -= dx;
                }
                else
                {
                    dx = XAxisInput;
                    XAxisInput = 0;
                }
            }

            float leftCheckingX = alliesController.LeftChar.position.x + dx;
            if (leftCheckingX <= (-Utilities.RUNNING_PATH_WIDTH / 2) + (Utilities.PATH_FENCE_WIDTH / 2) + (Utilities.NEUTRAL_ALLY_SIZE / 2))
            {
                dx = (-Utilities.RUNNING_PATH_WIDTH / 2) + (Utilities.PATH_FENCE_WIDTH / 2) + (Utilities.NEUTRAL_ALLY_SIZE / 2) - alliesController.LeftChar.position.x;
                XAxisInput = 0;
                currRotaion.y /= 2.0f;
            }

            if (XAxisInput >= -kcheckzero)
            {
                XAxisInput = 0;
            }
        }
        else if (XAxisInput > kcheckzero)//move right
        {
            type = 1;
            count4DesreaseRot = 0;
            IncreaseSpeed();
            if (PlayerPrefsUtil.IsMoveFollowATouch == 1)
            {
                CalculateRotation();
                float maxv1 = PlayerPrefsUtil.maxMoveWhen1;
                if (XAxisInput > maxv1)
                {
                    XAxisInput -= maxv1;
                    dx = maxv1;
                }
                else
                {
                    dx = XAxisInput;
                    XAxisInput = 0;
                }
            }
            else
            {
                dx = speedmoveHozi * Time.deltaTime;
                CalculateRotation();
                if (XAxisInput > dx)
                {
                    XAxisInput -= dx;
                }
                else
                {
                    dx = XAxisInput;
                    XAxisInput = 0;
                }
            }

            float rightCheckingX = alliesController.RightChar.position.x + dx;
            if (rightCheckingX >= (Utilities.RUNNING_PATH_WIDTH / 2) - (Utilities.PATH_FENCE_WIDTH / 2) - (Utilities.NEUTRAL_ALLY_SIZE / 2))
            {
                dx = (Utilities.RUNNING_PATH_WIDTH / 2) - (Utilities.PATH_FENCE_WIDTH / 2) - (Utilities.NEUTRAL_ALLY_SIZE / 2) - alliesController.RightChar.position.x;
                XAxisInput = 0;
                currRotaion.y /= 2.0f;
            }
            if (XAxisInput < kcheckzero)
            {
                XAxisInput = 0;
            }
        }

        ChangeRotation();
        for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
        {
            Transform character = alliesController.AlliesCrowd[i].transform;
            float newX = character.position.x + dx;
            character.position = new Vector3(newX, character.position.y, character.position.z);
            character.localEulerAngles = currRotaion;
        }

    }


    private void CalculateRotation()
    {
        if (XAxisInput > kcheckzero || XAxisInput < -kcheckzero)
        {
            CurrRotationY = 60.0f * XAxisInput;
            if (CurrRotationY > 70.0f)
            {
                CurrRotationY = 70.0f;
            }
            else if (CurrRotationY < -70.0f)
            {
                CurrRotationY = -70.0f;
            }
            else if (CurrRotationY < 25.0f && XAxisInput > 0)
            {
                CurrRotationY = 25.0f;
            }
            else if (CurrRotationY > -25.0f && XAxisInput < 0)
            {
                CurrRotationY = -25.0f;
            }
        }
        else
        {
            CurrRotationY = 0;
        }
    }


    private void ChangeRotation()
    {
        if (currRotaion.y > CurrRotationY)
        {
            if (CurrRotationY < 0 && currRotaion.y > 0)
            {
                currRotaion.y -= 360 * Time.deltaTime;
            }
            else
            {
                currRotaion.y -= 120 * Time.deltaTime;
            }
            if (currRotaion.y < CurrRotationY)
            {
                currRotaion.y = CurrRotationY;
            }
        }
        else if (currRotaion.y < CurrRotationY)
        {
            if (CurrRotationY > 0 && currRotaion.y < 0)
            {
                currRotaion.y += 360 * Time.deltaTime;
            }
            else
            {
                currRotaion.y += 120 * Time.deltaTime;
            }

            if (currRotaion.y > CurrRotationY)
            {
                currRotaion.y = CurrRotationY;
            }
        }
    }


    private void DecreaseRotation()
    {
        if (currRotaion.y > 0)
        {
            currRotaion.y -= 150 * Time.deltaTime;
            if (currRotaion.y < 0)
            {
                currRotaion.y = 0;
            }
            for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
            {
                Transform character = alliesController.AlliesCrowd[i].transform;
                character.localEulerAngles = currRotaion;
            }
        }
        else if (currRotaion.y < 0)
        {
            currRotaion.y += 150 * Time.deltaTime;
            if (currRotaion.y > 0)
            {
                currRotaion.y = 0;
            }
            for (int i = 0; i < alliesController.AlliesCrowd.Count; i++)
            {
                Transform character = alliesController.AlliesCrowd[i].transform;
                character.localEulerAngles = currRotaion;
            }
        }
    }

    float minksh = 0.8f;
    float maxksh = 2.0f;

    float minsh = 11;
    float maxsh = 16;
    float mindxin = 0.05f;
    float maxndxin = 1.0f;


    private void IncreaseSpeed()
    {
        float absxin = Mathf.Abs(XAxisInput);

        if (absxin <= 0.05f)
        {
            maxSpeedHori = minsh;
            kIncreaseSpeedHori = minksh;
        }
        else if (absxin >= 1)
        {
            maxSpeedHori = maxsh;
            kIncreaseSpeedHori = maxksh;
        }
        else
        {
            maxSpeedHori = (maxsh - minsh) * (absxin - mindxin) / (maxndxin - mindxin) + minsh;
            kIncreaseSpeedHori = (maxksh - minksh) * (absxin - mindxin) / (maxndxin - mindxin) + minksh;
        }
        if (speedmoveHozi < (kSpeedCurr * maxSpeedHori))
        {
            if (kIncreaseSpeedHori < 1)
            {
                speedmoveHozi += kIncreaseSpeedHori;
            }
            else
            {
                if (speedmoveHozi < 0.25f * maxSpeedHori)
                {
                    speedmoveHozi += kIncreaseSpeedHori * 0.8f;
                }
                else
                {
                    speedmoveHozi += kIncreaseSpeedHori;
                }
            }
            if (speedmoveHozi > (kSpeedCurr * maxSpeedHori))
            {
                speedmoveHozi = kSpeedCurr * maxSpeedHori;
            }
        }
    }
}
