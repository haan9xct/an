﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;

public class AlliesGatherer : MonoBehaviour
{
    [Header("References")]
    public GameObject selfStandardCollider;
    private AlliesController alliesController;
    //public Transform alliesGatheringFXPrefab;
    [SerializeField] Rigidbody rigidBody;
    [SerializeField] Collider myCollider;
    public void ResetObj()
    {
        rigidBody.isKinematic = true;
        myCollider.enabled = true;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ally Character"))
        {
            alliesController = other.transform.parent.parent.GetComponent<AlliesController>();
            selfStandardCollider.layer = LayerMask.NameToLayer(Utilities.ALLY_CHARACTER_LAYER);
            Debug.Log("vao");
            AddSelfToCrowd();
        }

    }
        public void AddSelfToCrowd()
    {
        transform.parent.parent = alliesController.transform;
        alliesController.OnCrowdGrowth();
        AudioManager.Instance.PlayOneShot("ConnectAllies", 0.7f);
        gameObject.SetActive(false);
    }
}
