﻿using PathologicalGames;
using UnityEngine;

public class NeutralSpawner : MonoBehaviour
{
    private void Start()
    {
        var character = PoolManager.Pools["Game"].Spawn("Neutral Char", transform.position, Quaternion.identity, transform.parent);
        character.GetComponent<AllyGeneral>().ResetCharacter();
        character.position = transform.position;
    }
}
