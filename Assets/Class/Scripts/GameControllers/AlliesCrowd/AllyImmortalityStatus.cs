﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyImmortalityStatus : MonoBehaviour
{
    private bool invulnerable = false;

	public bool Invulnerable { get => invulnerable; set => invulnerable = value; }
}
