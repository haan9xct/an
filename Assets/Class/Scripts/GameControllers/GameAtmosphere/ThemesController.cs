﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ThemesController : MonoBehaviour
{
    public static ThemesController Instance;

    public Material maincharmat;
    public List<Material> allyMaterials;
    public int currentIndexAllyMat;
    public List<Material> allyBloodFXMaterials;
    public List<Material> allyDeathFXMaterials;
    public Material[] colorDeath;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        ResetColorPalette();
    }


    public void ResetColorPalette()
    {
        int randAllyColor = Random.Range(0, allyMaterials.Count);
        currentIndexAllyMat = randAllyColor;
        maincharmat.color = allyMaterials[randAllyColor].color;
        foreach (var mat in allyBloodFXMaterials)
        {
            mat.color = colorDeath[randAllyColor].color;
        }

        foreach (var mat in allyDeathFXMaterials)
        {
            mat.color = colorDeath[randAllyColor].color;
        }
    }
}
