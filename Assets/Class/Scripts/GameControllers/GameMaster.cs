﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Cinemachine;
using DG.Tweening;
using mygame.sdk;
using UnityEngine.SceneManagement;
using System.Collections.Generic;


public class GameMaster : MonoBehaviour
{
    public static GameMaster Instance;

    [Header("References")]
    [SerializeField] UIController uiController;
    public AlliesController alliesController;
    public CameraController cameraController;
    public Transform directionLight;
    [Header("Status parameters")]
    public static GameStates gameStates;
    public static EndgameStates endgameStates;
    public Transform poolTransform;

    public bool isBonusLevel;
    float roadLength;
    public float camZ = -16;
    public float camAngle = 26;

    public bool isTurnOnAudio;
    public Transform posCoinUI;
    public float RoadLength { get => roadLength; }
    public bool canOpenGift;
    public bool isLosewhenRun { get; set; }
    public Vector3 currentAngle;

    private void Awake()
    {
        //Application.targetFrameRate = 60;
        Instance = this;
        isLosewhenRun = false;
        Input.multiTouchEnabled = false;
    }

    public void EndGame()
    {
        AudioManager.Instance.Stop("stop");
        StartCoroutine(DelayLoadScene(2.4f));

    }
    IEnumerator DelayLoadScene(float f)
    {
        GameMaster.gameStates = GameStates.ENDGAME;

        yield return new WaitForSeconds(f);
        SceneManager.LoadScene((0));

    }
    private void Start()
    {
        if (mygame.sdk.PlayerPrefsUtil.MusicSetting == 0)
        {
            isTurnOnAudio = true;
        }
        else
        {
            isTurnOnAudio = false;
        }

        float fff = (float)Screen.height / (float)Screen.width;
        if (fff < 1.778f)
        {
            fff = 1.778f;
        }
        else if (fff > 2.1643f)
        {
            fff = 2.1643f;
        }
        camZ = -20 + 4 * (fff - 2.1643f) / (1.778f - 2.1643f);
        camAngle = 26 + 2 * (fff - 1.778f) / (2.1643f - 1.778f);
        uiController.panelStart.GetComponent<ButtonController>().CallFunc = ClickToStartGame;

    }


    private void Update()
    {
        if (gameStates == GameStates.MENU)
        {
            uiController.tutorial.SetActive(true);
        }
        else
        {
            uiController.tutorial.SetActive(false);
        }
    }


    public void ClickToStartGame()
    {
        cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_XDamping = 1.8f;
        cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_YDamping = 0.8f;
        cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_ZDamping = 0.8f;
        if (!UIController.Instance.isWait4NewPlay && gameStates != GameStates.INGAME)
        {
            if (mygame.sdk.PlayerPrefsUtil.memIdxbonusMap <= -2)
            {
                string lv4log = $"level_{PlayerPrefsUtil.Level:000}_play";
            }
            else
            {
                int lv = PlayerPrefsUtil.Level - 1;
                string lv4log = $"level_{lv:000}_play_bonus";
            }
            uiController.panelStart.SetActive(false);
            roadLength = 0;
            isLosewhenRun = false;
            uiController.OpenIngameUI();
            alliesController.UpdateAllCrowdData();
            GameMaster.gameStates = GameStates.INGAME;
            if (mygame.sdk.PlayerPrefsUtil.memIdxbonusMap > -2)
            {
                foreach (var item in LevelController.Instance.listCurr)
                {
                    roadLength = item.length;
                }
            }
            else
            {

                // for (int i = 0; i < LevelController.Instance.listCurr.Count; i++)
                // {
                //     if (i == 0)
                //     {
                //         roadLength = LevelController.Instance.listCurr[i].posStart * 6.25f + LevelController.Instance.listCurr[i].length;
                //     }
                //     else
                //     {
                //         roadLength += LevelController.Instance.listCurr[i].length;
                //     }
                // }

            }

           
            if (!GameMaster.Instance.isBonusLevel)
            {
                StartCoroutine(followCameraPlay());
            }
        }

    }


    public void Reset()
    {
        Instance.GetComponent<ThemesController>().ResetColorPalette();

        // Reset camera
        cameraController.Reset();

        // Characters reset
        alliesController.Reset();
        if (alliesController.AlliesCrowd[0].GetComponent<HeroEquipment>() != null)
        {
        }
        setCameraWaitPlay();
    }

   
    IEnumerator followCameraPlay()
    {
        float tcurr = 0f;
        float dua = 1.0f;
        float cygo = 14;
        while (tcurr < dua)
        {
            tcurr += Time.deltaTime;
            float goc = 10 + tcurr * (camAngle - 10) / dua;
            float cy = 5 + tcurr * (cygo - 5.0f) / dua;
            if (goc > camAngle)
            {
                goc = camAngle;
                yield break;
            }
            cameraController.cinemachineVirtual.transform.localEulerAngles = new Vector3(goc, 0, 0);
            float cz = cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset.z;
            cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = new Vector3(0, cy, cz);
            yield return null;
        }
        cameraController.cinemachineVirtual.transform.localEulerAngles = new Vector3(camAngle, 0, 0);
        float czf = cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset.z;
        cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = new Vector3(0, cygo, czf);

    }


    public void setCameraWaitPlay()
    {
        
            cameraController.cinemachineVirtual.m_Lens.FieldOfView = 60;
            cameraController.cinemachineVirtual.transform.localEulerAngles = new Vector3(10, 0, 0);
            cameraController.cinemachineVirtual.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = new Vector3(0, 5, camZ);
        
    }


   


    public void OnAlliesMoving()
    {
        cameraController.OnAlliesMoving();
    }


    
}


public enum GameStates
{
    MENU,
    INGAME,
    ENDGAME
}
