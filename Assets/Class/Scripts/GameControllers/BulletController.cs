﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BulletController : MonoBehaviour
{
    Vector3 direction;
    bool isShotgun;
    float speed = 1;
    public void Move(Vector3 _direction, float _speed, MoveType moveType, LayerMask layer)
    {
        if (moveType == MoveType.Normal)
        {
            this.direction = _direction;
        }
        else if (moveType == MoveType.UnAccurate)
        {
            this.direction = new Vector3(Random.Range(_direction.x - 0.1f, _direction.x + 0.1f), _direction.y - 0.05f, _direction.z);
        }
        speed = _speed;
        gameObject.layer = layer;
    }
   
    private void Update()
    {
        this.transform.position += direction * speed * Time.deltaTime;
    }

}
public enum MoveType
{
    Normal, Missle, UnAccurate
}