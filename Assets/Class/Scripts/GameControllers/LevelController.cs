﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame.sdk;
using Random = System.Random;
using DG.Tweening;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;

    [Header("Testing parameters")]

    public AlliesController alliesController;

    public ItemMapModel prefabStart;

    public List<BattleController> listCastle;

    public List<ItemMapModel> mapStart;

    [HideInInspector]
    public List<BaseEnemyObj> ListEnemys;
    [HideInInspector]
    public BaseEnemyObj alliesInfo;
    private List<ItemMapModel> listEndHard1;
    private List<ItemMapModel> listEndHard2;
    private List<ItemMapModel> listEndHard3;
    private List<ItemMapModel> listEndHard4;

    private Dictionary<string, GameObject> dicMaps = new Dictionary<string, GameObject>();

    [HideInInspector]
    public BattleController currBattle;

    public float boxLength = 6.25f;
    public List<ItemMapModel> listCurr;

    private int idxCastleCurr;

    private int count4Hard = 0;

    public int stateEndGame4Next = -1;//0-lose; 1-next level, 2-second play

    public float kSubBlood = .2f;
    [HideInInspector]
    public float lenghtCurrMap = 0;
    bool isLowRam = false;


    private void Awake()
    {
        Debug.Log(ListEnemys);
        PlayerPrefsUtil.Level = 3;
        Instance = this;
        listEndHard1 = new List<ItemMapModel>();
        listEndHard2 = new List<ItemMapModel>();
        listEndHard3 = new List<ItemMapModel>();
        listEndHard4 = new List<ItemMapModel>();
        listCurr = new List<ItemMapModel>();
        ListEnemys = new List<BaseEnemyObj>();
        alliesInfo = GetComponent<BaseEnemyObj>();
        idxCastleCurr = PlayerPrefsUtil.memCastleCurr;
        isLowRam = false;
    }


    void Start()
    {
            TryLevel(PlayerPrefsUtil.Level, true);
        GameMaster.Instance.setCameraWaitPlay();
#if UNITY_IOS
        long memorylimit = 0;
#if !UNITY_EDITOR

			memorylimit = mygame.sdk.GameHelperIos.GetMemoryLimit();
#endif
        long memcheck = 900 * 1024 * 1024;
        if ((memorylimit < memcheck && memorylimit <= 0))
        {
            isLowRam = true;
        }
#endif
    }


    public void Prepe4Play()
    {
        TryLevel(PlayerPrefsUtil.Level);
    }


    public void TryLevel(int level, bool isStart = false)
    {
        if (PlayerPrefsUtil.memIdxbonusMap > -2)
        {
            InstantiateMap();
            return;
        }

        if (listCurr.Count <= 0)
        {
            LoadMapifHasGen(level);
        }
       // LoadArmyIfHasGen(level);
       // InstantiateMap();
    }

    public void LoadMapifHasGen(int level)
    {
        if (level <= mapStart.Count && PlayerPrefsUtil.IsSecondPlay == 0)
        {
            listCurr.Clear();
            listCurr.Add(mapStart[level - 1]);
        }
        else
        {
            string memmaplv = PlayerPrefsUtil.MemMapgen;
            if (memmaplv.Length > 3)
            {
                addMappFromString(memmaplv);
            }
            else
            {
                if (PlayerPrefsUtil.IsSecondPlay == 1)
                {
                    GenSecondLevel(level);
                }
                else
                {
                    GenStartLevel(level);
                }
            }
        }
        idxCastleCurr = PlayerPrefsUtil.memCastleCurr;
        if (idxCastleCurr < 0)
        {
            GenCastle();
        }
    }


    private void SaveListInfo4Battle()
    {
        string sdata = "{\"enemys\":[";

        for (int i = 0; i < ListEnemys.Count; i++)
        {
            string sitem = ListEnemys[i].toString();
            if (i == 0)
            {
                sdata += sitem;
            }
            else
            {
                sdata += ("," + sitem);
            }
        }

        sdata += "],\"allies\":{";
        sdata += $"\"blood\":{alliesInfo.blood},";
        sdata += $"\"power\":{alliesInfo.power},";
        sdata += $"\"armor\":{alliesInfo.armor},";
        sdata += $"\"originBlood\":{alliesInfo.originBlood}";
        sdata += "}}";
        PlayerPrefsUtil.memdataenemy = sdata;
    }


    private void GenEnemy(int level)
    {
        ListEnemys.Clear();
        float kup = 1.4f;
        if (level >= 25)
        {
            kup = 1.65f;
        }
        float totalep = 0;
        float totaleb = 0;
        if (level % 3 == 0)
        {
            int num = 5 + level;
            int enemyblood = 10 + (level / 10) * 2 + PlayerPrefsUtil.enemyLvArmor;
            int enemypower = 10 + (level / 10) * 2 + PlayerPrefsUtil.enemyLvPower;
            BaseEnemyObj boss = new BaseEnemyObj();
            boss.typeEnemy = 1;
            if (PlayerPrefsUtil.TypeBattleFight == 1)
            {
                boss.power = enemypower * num;
            }
            else
            {
                boss.power = enemypower * num * 0.8f;
            }
            boss.blood = enemyblood * num;

            boss.armor = ((level / 10) + PlayerPrefsUtil.enemyLvArmor) * 1.5f;
            boss.originBlood = boss.blood;
            ListEnemys.Add(boss);
            totalep = boss.power;
            totaleb = boss.blood;
        }
        else
        {
            int num = 5 + level;
            int memnem = num;
            float dnum = 0;
            if (num > 40)
            {
                num = UnityEngine.Random.Range(35, 42);
                dnum = memnem - num;
            }
            if (level >= 25)
            {
                int rrr = UnityEngine.Random.Range(0, 100);
                if (rrr < 30)
                {
                    num = 2 * num / 3;
                }
            }
            else if (level >= 15)
            {
                int rrr = UnityEngine.Random.Range(0, 100);
                if (rrr < 20)
                {
                    num = 2 * num / 3;
                }
            }
            for (int i = 0; i < num; i++)
            {
                BaseEnemyObj enemy = new BaseEnemyObj();
                enemy.typeEnemy = 0;
                enemy.blood = 10 + (level / 10) * 2 + PlayerPrefsUtil.enemyLvArmor;
                enemy.blood += dnum * enemy.blood / num;
                enemy.power = 10 + (level / 10) * 2 + PlayerPrefsUtil.enemyLvPower;
                enemy.power += dnum * enemy.power / num;
                enemy.armor = (level / 10) + PlayerPrefsUtil.enemyLvArmor;
                enemy.originBlood = enemy.blood;
                ListEnemys.Add(enemy);
                totalep += enemy.power;
                totaleb += enemy.blood;
            }
        }
        if (level <= 15)
        {
            alliesInfo.blood = 10 + (level / 10) * 2;
            alliesInfo.power = 10 + (level / 10) * 2;
            alliesInfo.armor = (level / 10) + 1;
        }
        else
        {
            if (PlayerPrefsUtil.Level >= 10)
            {
                float PlayerLvRequire = getRequireLvPlayer(level);
                float ablood = totaleb - PlayerLvRequire * 10 + 5;
                float apower = totalep - PlayerLvRequire * 10;

                if (level % 3 == 0)
                {
                    float numrq = 5 + level;
                    if (numrq > 35)
                    {
                        numrq = 35;
                    }
                    if (PlayerPrefsUtil.TypeBattleFight != 1)
                    {
                        apower = apower * 5.0f / 4.0f;
                    }
                    alliesInfo.blood = ablood / numrq;
                    alliesInfo.power = apower / numrq;
                    alliesInfo.armor = (level / 10) + PlayerPrefsUtil.enemyLvArmor;
                    if (PlayerPrefsUtil.IsUpInfoAlly == 1)
                    {
                        float ipower = totalep / numrq;
                        float iblood = totaleb / numrq;
                        if (ipower / alliesInfo.power > kup)
                        {
                            alliesInfo.power = ipower / kup;
                        }
                        if (iblood / alliesInfo.blood > kup)
                        {
                            alliesInfo.blood = iblood / kup;
                        }
                    }
                }
                else
                {
                    float numrq = ListEnemys.Count;
                    if (numrq > 35)
                    {
                        numrq = 35;
                    }
                    alliesInfo.blood = ablood / numrq;
                    alliesInfo.power = apower / numrq;
                    alliesInfo.armor = ListEnemys[0].armor;

                    if (PlayerPrefsUtil.IsUpInfoAlly == 1)
                    {
                        if (ListEnemys[0].power / alliesInfo.power > kup)
                        {
                            alliesInfo.power = ListEnemys[0].power / kup;
                        }
                        if (ListEnemys[0].blood / alliesInfo.blood > kup)
                        {
                            alliesInfo.blood = ListEnemys[0].blood / kup;
                        }
                    }
                }
            }
            else
            {
                alliesInfo.blood = 0.9f * (10 + (level / 10) * 2);
                alliesInfo.power = 0.9f * (10 + (level / 10) * 2);
                alliesInfo.armor = 0.9f * ((level / 10) + 1);
            }
        }
    }


    private int getRequireLvPlayer(int level)
    {
        int re = 1;
        if (level <= 30)
        {//1-30: 1.25
            re = (int)((float)level / 1.25f);
        }
        else if (level <= 50)
        {//31-50:1.5
            re = 24 + (33 - 24) * (level - 31) / (50 - 31);
        }
        else if (level <= 80)
        {//51-80: 1.9
            re = 34 + (42 - 34) * (level - 51) / (80 - 51);
        }
        else
        {
            re = 43 + (level - 80) / 4;
        }

        return re;
    }


    public void afterEditInfo4Display(float ab, float aar, float ap, float eb, float ear, float ep)
    {
        for (int i = 0; i < ListEnemys.Count; i++)
        {
            ListEnemys[0].originBlood = eb;
            ListEnemys[0].blood = eb;
            ListEnemys[0].armor = ear;
            ListEnemys[0].power = ep;
        }

        alliesInfo.originBlood = ab;
        alliesInfo.blood = ab;
        alliesInfo.armor = aar;
        alliesInfo.power = ap;
        SaveListInfo4Battle();
    }
    public void OnWinning()
    {
        stateEndGame4Next = 1;
        PlayerPrefsUtil.Level++;
        PlayerPrefsUtil.memdataenemy = "";
        PlayerPrefsUtil.memCastleCurr = -1;
        idxCastleCurr = -1;
        PlayerPrefsUtil.MemMapgen = "";
        PlayerPrefsUtil.IsSecondPlay = 0;
        PlayerPrefsUtil.memGunBoss = -1;
        listCurr.Clear();
        if (PlayerPrefsUtil.Level > 1 && PlayerPrefsUtil.Level % 3 == 1)
        {
            PlayerPrefsUtil.memIdxbonusMap = -1;
        }
        if (PlayerPrefsUtil.enemyLvPowerDeltaic > 0)
        {
            PlayerPrefsUtil.enemyLvPower += PlayerPrefsUtil.enemyLvPowerDeltaic;
            
        }
       
       
    }


    public void OnFightingLose(List<BaseEnemyObj> list)
    {
        stateEndGame4Next = 2;
        PlayerPrefsUtil.MemMapgen = "";
        PlayerPrefsUtil.IsSecondPlay = 1;
        listCurr.Clear();
        LevelController.Instance.ChangeEnemyListWhenLose(list);
    }


    public void ChangeEnemyListWhenLose(List<BaseEnemyObj> list)
    {
        ListEnemys.Clear();
        ListEnemys.AddRange(list);
        SaveListInfo4Battle();
    }


    private void SaveListCurr()
    {
        if (listCurr.Count > 0)
        {
            string sdata = "";
            for (int i = 0; i < listCurr.Count; i++)
            {
                if (sdata.Length > 0)
                {
                    int val = ((int)listCurr[i].type) * 1000 + listCurr[i].idxinList;
                    sdata += $",{val}";
                }
                else
                {
                    int val = ((int)listCurr[i].type) * 1000 + listCurr[i].idxinList;
                    sdata += $"{val}";
                }
            }
            PlayerPrefsUtil.MemMapgen = sdata;
            PlayerPrefsUtil.memCastleCurr = idxCastleCurr;
        }
    }


    public void GenCastle()
    {
        if (PlayerPrefsUtil.Level > 3 * listCastle.Count)
        {
            Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
            idxCastleCurr = ran.Next(0, listCastle.Count);
        }
        else
        {
            idxCastleCurr = (PlayerPrefsUtil.Level - 1) / 3;
        }
    }


    public void GenStartLevel(int level)
    {
        if (level <= mapStart.Count)
        {
            listCurr.Clear();
            listCurr.Add(mapStart[level - 1]);
        }
        else
        {
            listCurr.Clear();
            string cfMap = PlayerPrefs.GetString("cf_map_level_" + level);
            addMappFromString(cfMap);
            if (listCurr.Count <= 0)
            {
                Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
                listCurr.Clear();
                ItemMapModel itemend;
                if (level <= 15)
                {
                    itemend = genItemMapEnd4StartLevel(true, level);
                }
                else
                {
                    if (level <= 20)
                    {
                        if (PlayerPrefsUtil.Count4Hard3 < 4)
                        {
                            PlayerPrefsUtil.Count4Hard3++;
                            int per = ran.Next(0, 100);
                            if (per < (PlayerPrefsUtil.Count4Hard3 * 18))
                            {
                                PlayerPrefsUtil.Count4Hard3 = 0;
                                int idx = ran.Next(listEndHard3.Count);
                                itemend = listEndHard3[idx];
                            }
                            else
                            {
                                int idx = ran.Next(listEndHard1.Count + listEndHard2.Count);
                                if (idx < listEndHard1.Count)
                                {
                                    itemend = listEndHard1[idx];
                                }
                                else
                                {
                                    idx -= listEndHard1.Count;
                                    itemend = listEndHard2[idx];
                                }
                            }
                        }
                        else
                        {
                            PlayerPrefsUtil.Count4Hard3 = 0;
                            int idx = ran.Next(listEndHard3.Count);
                            itemend = listEndHard3[idx];
                        }
                    }
                    else
                    {
                        if (PlayerPrefsUtil.Count4Hard4 < 4)
                        {
                            PlayerPrefsUtil.Count4Hard4++;
                            int per = ran.Next(0, 100);
                            if (per < (PlayerPrefsUtil.Count4Hard4 * 20))
                            {
                                PlayerPrefsUtil.Count4Hard4 = 0;
                                int idx = ran.Next(listEndHard4.Count);
                                itemend = listEndHard4[idx];
                            }
                            else
                            {
                                int idx = ran.Next(listEndHard1.Count + listEndHard2.Count + listEndHard3.Count);
                                if (idx < listEndHard1.Count)
                                {
                                    itemend = listEndHard1[idx];
                                }
                                else
                                {
                                    idx -= listEndHard1.Count;
                                    if (idx < listEndHard2.Count)
                                    {
                                        itemend = listEndHard2[idx];
                                    }
                                    else
                                    {
                                        idx -= listEndHard2.Count;
                                        itemend = listEndHard3[idx];
                                    }
                                }
                            }
                        }
                        else
                        {
                            PlayerPrefsUtil.Count4Hard4 = 0;
                            int idx = ran.Next(listEndHard4.Count);
                            itemend = listEndHard4[idx];
                        }
                    }

                }

                int numitemmap = GetNumitemMap(level);
                // if (level <= 8 && (itemend.type == Map_item_type.Speed || itemend.type == Map_item_type.Jumping))
                // {
                //     int idx = ran.Next(listEndHard1.Count);
                //     itemend = listEndHard1[idx];
                // }
                GenListMapCurr(level, numitemmap, itemend, (level > 8));
            }
        }
        if (idxCastleCurr < 0)
        {
            GenCastle();
        }
        //SaveListCurr();
    }


    private void addMappFromString(string mapdata)
    {
        if (mapdata != null && mapdata.Length > 10)
        {
            string[] arr = mapdata.Split(new char[] { ',' });
            for (int i = 0; i < arr.Length; i++)
            {
                int val = int.Parse(arr[i]);
                int type = val / 1000;
                int idx = val % 1000;
                if (type == 0)
                {
                    listCurr.Add(prefabStart);
                }
            }
        }
    }


    public void GenSecondLevel(int level)
    {
        Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
        listCurr.Clear();
        ItemMapModel itemend = null;
        if (level <= 15)
        {
            itemend = genItemMapEnd4StartLevel(false, level);
        }
        else
        {
            if (level <= 30)
            {
                int idx = ran.Next(listEndHard1.Count);
                itemend = listEndHard1[idx];
            }
            else
            {
                int idx = ran.Next(listEndHard2.Count);
                itemend = listEndHard2[idx];
            }
        }

        int numitemmap = GetNumitemMap(level);
        GenListMapCurr(level, numitemmap, itemend, (level > 8));
        if (idxCastleCurr < 0)
        {
            GenCastle();
        }
        SaveListCurr();
    }


    private ItemMapModel genItemMapEnd4StartLevel(bool isGenStart, int level)
    {
        Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
        ItemMapModel itemend = null;
        List<ItemMapModel> listTmp = null;
        if (isGenStart)
        {
            if (level <= 10)
            {
                listTmp = listEndHard1;
            }
            else
            {
                if (PlayerPrefsUtil.Count4Hard3 < 4)
                {
                    PlayerPrefsUtil.Count4Hard3++;
                    int per = ran.Next(0, 100);
                    if (per < (PlayerPrefsUtil.Count4Hard3 * 18))
                    {
                        PlayerPrefsUtil.Count4Hard3 = 0;
                        listTmp = listEndHard3;
                    }
                    else
                    {
                        int idx = ran.Next(listEndHard1.Count + listEndHard2.Count);
                        if (idx < listEndHard1.Count)
                        {
                            listTmp = listEndHard1;
                        }
                        else
                        {
                            listTmp = listEndHard2;
                        }
                    }
                }
                else
                {
                    PlayerPrefsUtil.Count4Hard3 = 0;
                    listTmp = listEndHard3;
                }
            }
        }
        else
        {
            listTmp = listEndHard1;
        }
        List<int> lllidx = new List<int>();
        for (int i = 0; i < listTmp.Count; i++)
        {
            lllidx.Add(i);
        }
        while (itemend == null && lllidx.Count > 0)
        {
            int idx = ran.Next(lllidx.Count);
            if (listTmp[lllidx[idx]].isUse4StartLevel)
            {
                if (!isGenStart && level <= 8)
                {
                    if (listTmp[lllidx[idx]].type != Map_item_type.Speed)
                    {
                        itemend = listTmp[lllidx[idx]];
                        break;
                    }
                    else
                    {
                        lllidx.RemoveAt(idx);
                    }
                }
                else
                {
                    itemend = listTmp[lllidx[idx]];
                    break;
                }
            }
            else
            {
                lllidx.RemoveAt(idx);
            }
        }
        return itemend;
    }


    private void GenListMapCurr(int level, int numitemmap, ItemMapModel itemend, bool hasSpeed)
    {
        List<int> list4gen = new List<int>();
       
        Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
        int idxspeed = -1;
        int idxspeedinmap = -1;
        
        listCurr.Clear();
        listCurr.Add(itemend);
        ItemMapModel itcurr = itemend;
        // float tolen = (float)itemend.length / boxLength;
        for (int i = (numitemmap - 2); i >= 0; i--)
        {
            ItemMapModel itemwilladd;
           
          
        }
        int minlen = 22;
        if (level > 40)
        {
            minlen = 40;
        }
        else if (level > 30)
        {
            minlen = 34;
        }
        else if (level > 20)
        {
            minlen = 28;
        }
        else if (level > 10)
        {
            minlen = 24;
        }

    }
    private void InstantiateMap()
    {
        int n = transform.childCount;
        for (int i = (n - 1); i >= 1; i--)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        lenghtCurrMap = 0;

        float xcurr = 0;
        bool isKey = false;
        bool isGift = false;
        bool isBattleItem = false;
        int itemType = -1;
        for (int i = 0; i < listCurr.Count; i++)
        {
            float dx;
            if (i == 0)
            {
                dx = listCurr[i].posStart * boxLength + listCurr[i].length / 2;
            }
            else
            {
                dx = (listCurr[i - 1].length + listCurr[i].length) / 2;
            }
            lenghtCurrMap += listCurr[i].length;
            Vector3 posSpawn = new Vector3(0, 0, xcurr + dx);
           
                gameObject.transform.GetChild(0).gameObject.SetActive(true);
                var itemins = InstantiateItemMap(listCurr[i].pathRes, posSpawn, transform);
                listCurr[i].itemAnchor = itemins.GetComponent<ItemMapModel>().itemAnchor;
            

            if (listCurr[i].itemAnchor != null)
            {
                if (PlayerPrefsUtil.Level > 15)
                {
                    bool hasKey = false;
                    bool hasGift = false;
                    if (!isKey && !isGift)
                    {
                        int ran = UnityEngine.Random.Range(0, 100);
                        int perget = PlayerPrefsUtil.PerGetKeyAndGift;
                        if (PlayerPrefsUtil.countKeyGift >= 3)
                        {
                            perget = 100;
                        }
                        else
                        {
                            perget = PlayerPrefsUtil.PerGetKeyAndGift - (3 - PlayerPrefsUtil.countKeyGift) * 7;
                        }
                        if (ran <= perget)
                        {
                            PlayerPrefsUtil.countKeyGift = 0;
                            ran = UnityEngine.Random.Range(0, 100);
                            if (ran <= 50)
                            {
                                isGift = true;
                                hasGift = true;
                            }
                            else
                            {
                                isKey = true;
                                hasKey = true;
                            }
                        }
                    }
                    else if (!isGift || !isGift)
                    {
                        int ran = UnityEngine.Random.Range(0, 100);
                        int perget = PlayerPrefsUtil.PerGetOnlyKeyOrGift - (3 - PlayerPrefsUtil.countKeyGift) * 2;
                        if (ran <= perget)
                        {
                            if (!isKey)
                            {
                                isKey = true;
                                hasKey = true;
                            }
                            else if (!isGift)
                            {
                                isGift = true;
                                hasGift = true;
                            }
                        }
                    }
                  

                    if (!hasGift && !hasKey && !isBattleItem)
                    {
                        if (UnityEngine.Random.Range(0, 100) < 50)
                        {
                            PlayerPrefsUtil.CountBattleItem = 0;
                            isBattleItem = true;
                            int ranItem = UnityEngine.Random.Range(0, 100);
                            if (ranItem < 40)
                            {
                                itemType = 0;
                            }
                            else if (ranItem > 40 && ranItem < 80)
                            {
                                itemType = 1;
                            }
                            else
                            {
                                itemType = 2;
                            }
                        }
                        else
                        {
                            PlayerPrefsUtil.CountBattleItem++;
                        }

                        if (itemType > -1)
                        {
                        }
                    }


                }
            }
            xcurr += dx;
        }

       


        float lenend = listCurr[listCurr.Count - 1].length / 2;
        if (PlayerPrefsUtil.memIdxbonusMap == -2)
        {

                    Debug.Log("Boss");

            if (idxCastleCurr < 0 || idxCastleCurr >= listCastle.Count)
            {
                GenCastle();
            }
            var obins = InstantiateItemMap(listCastle[idxCastleCurr].pathRes, new Vector3(0, 0, xcurr + lenend), transform);
            Debug.Log(obins);
            currBattle = obins.GetComponent<BattleController>();//Instantiate1(listCastle[idxCastleCurr], new Vector3(0, 0, xcurr + lenend), Quaternion.identity, transform);
            EndgameController.Instance.battleController = currBattle;
            for (int i = 0; i < ListEnemys.Count; i++)
            {   

                if (ListEnemys[i].typeEnemy == 0)
                {

                    currBattle.SpawnBoss(ListEnemys[i]);
                    break;
                }
            }
        }
        #region Show environment elements
        float roadLength = 0;
        if (mygame.sdk.PlayerPrefsUtil.memIdxbonusMap > -2)
        {
            foreach (var item in listCurr)
            {
                roadLength = item.length;
            }
        }
        else
        {

            for (int i = 0; i < listCurr.Count; i++)
            {
                if (i == 0)
                {
                    roadLength = LevelController.Instance.listCurr[i].posStart * boxLength + LevelController.Instance.listCurr[i].length;
                }
                else
                {
                    roadLength += LevelController.Instance.listCurr[i].length;
                }
            }

        }
        #endregion
    }


    private GameObject InstantiateItemMap(string pathRes, Vector3 pos, Transform parent)
    {
        GameObject objectmap;
        if (dicMaps.ContainsKey(pathRes))
        {
            objectmap = dicMaps[pathRes];
        }
        else
        {
            objectmap = Resources.Load<GameObject>(pathRes);
            if (!isLowRam)
            {
                dicMaps.Add(pathRes, objectmap);
            }

        }
        GameObject re = Instantiate(objectmap, pos, Quaternion.identity, parent);
        if (isLowRam)
        {
            Destroy(objectmap);
        }
        return re;
    }


    private int GetNumitemMap(int level)
    {
        Random ran = new Random((int)SdkUtil.systemCurrentMiliseconds());
        int re = 5;
        if (level < 20)
        {
            re = 4;
        }
        else if (level < 40)
        {
            re = 4;
        }
        else if (level < 60)
        {
            re = ran.Next(4, 6);
        }
        else if (level < 80)
        {
            re = ran.Next(5, 6);
        }
        else
        {
            re = ran.Next(5, 8);
        }

        return re;
    }

}
