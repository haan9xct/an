﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Map_item_type
{
    Module_empty2 = 0,
    Jumping,
    Module,
    Speed,
    Turning
}

[Serializable]
public class ItemMapModel : MonoBehaviour
{
    public Map_item_type type = Map_item_type.Module;

    [HideInInspector]
    public int idxinList = 0;

    public float length;
    public bool isOverBegin = false;
    public bool isEmptyEnd = true;
    public bool isUse4StartLevel = false;
    public int hardlevel = 1;
    public int posStart = 2;
    public int numAlly;
    public int perAllyGetAble;
    public int perAllyPass;
    public int numEnemy = 0;
    public string pathRes = "Modules/";
    public Transform itemAnchor;
}
