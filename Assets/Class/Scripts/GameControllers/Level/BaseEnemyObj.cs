﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemyObj : MonoBehaviour
{
    public int typeEnemy;//0 small enemy, 1-boss
    public float blood;
    public float power;
    public float armor;
    [HideInInspector]
    public float originBlood;
    public void ResetObj()
    {
        blood = 10;
        power = 10;
        armor = 0;
    }
    public string toString()
    {
        string sdata = "{" + $"\"typeEnemy\":{typeEnemy},\"blood\":{blood},\"power\":{power},\"armor\":{armor},\"originBlood\":{originBlood}" + "}";
        return sdata;
    }

    public void copyData(BaseEnemyObj other)
    {
        typeEnemy = other.typeEnemy;
        blood = other.blood;
        power = other.power;
        armor = other.armor;
        originBlood = other.originBlood;
    }
}
