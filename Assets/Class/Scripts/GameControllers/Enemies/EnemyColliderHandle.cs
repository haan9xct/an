﻿using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class EnemyColliderHandle : MonoBehaviour
{
    [SerializeField] GameObject bloodFX;
    private BloodFXDelayer bloodFXDelayer;

    private void Start()
    {
        bloodFXDelayer = GameMaster.Instance.GetComponent<BloodFXDelayer>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("AllyBullet"))
        {
            if (bloodFXDelayer.IncreasePlayingEnemyBloodFx())
            {
                Transform blood = PoolManager.Pools["Game"].Spawn(bloodFX, transform.position + Vector3.up * 1.5f, Quaternion.identity);
                PoolManager.Pools["Game"].Despawn(blood, 2f);
            }

            PoolManager.Pools["Game"].Despawn(other.transform);
        }
    }
}
