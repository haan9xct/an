﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodFX : MonoBehaviour
{
    [SerializeField] ParticleSystemRenderer[] subBloodMaterial;
    [SerializeField] ParticleSystem sparks;
    public void SetColor(Material material, Color color)
    {
        for (int i = 0; i < subBloodMaterial.Length; i++)
        {
            // subBloodMaterial[1].material = material;
        }
        ParticleSystem.MainModule mainModule = sparks.main;
        mainModule.startColor = color;
        GetComponent<ParticleSystem>().Play();
    }
}
