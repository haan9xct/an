﻿using System.Collections;
using PathologicalGames;
using UnityEngine;
using DG.Tweening;
public class CharactersDeathFX : MonoBehaviour
{
    public enum CharType
    {
        Neutral,
        Allied,
        Enemy
    }

    [Header("Dieing FX Materials")]
    public Material neutralChar1;
    public Material neutralChar2;
    public Material alliedChar1;
    public Material alliedChar2;
    public Material enemyChar1;
    public Material enemyChar2;

    [Header("Death FX Parameters")]
    public Transform deathFXPrefab;
    public float fxScale = 1f;
    long tPlaySound = 0;

    public GameObject coinPrefab;
    public GameObject explosion;
   

    public void DisplayDeathFX(CharType charType,bool isPool)
    {
        transform.Find("Mesh").gameObject.SetActive(false);
        if (transform.Find("FirePoint") != null)
        {
            transform.Find("FirePoint").gameObject.SetActive(false);
        }
        Transform fx = PoolManager.Pools["Game"].Spawn(deathFXPrefab, transform.position, Quaternion.identity);
        fx.localScale = Vector3.one * fxScale;

        switch (charType)
        {
            case CharType.Neutral:
                ParticleSystemRenderer main1 = fx.GetComponent<ParticleSystemRenderer>();
                // main1.material = neutralChar1;
                ParticleSystemRenderer sub1 = fx.GetChild(0).GetComponent<ParticleSystemRenderer>();
                // sub1.material = neutralChar2;
                break;
            case CharType.Allied:
                ParticleSystemRenderer main2 = fx.GetComponent<ParticleSystemRenderer>();
                // main2.material = alliedChar1;
                ParticleSystemRenderer sub2 = fx.GetChild(0).GetComponent<ParticleSystemRenderer>();
                // sub2.material = alliedChar2;
                break;
            case CharType.Enemy:
                ParticleSystemRenderer main3 = fx.GetComponent<ParticleSystemRenderer>();
                // main3.material = enemyChar1;
                ParticleSystemRenderer sub3 = fx.GetChild(0).GetComponent<ParticleSystemRenderer>();
                // sub3.material = enemyChar2;

                break;
            default:
                break;
        }
        if (transform.GetComponentInChildren<EnemyColliderHandle>() != null)
        {
            transform.GetComponentInChildren<EnemyColliderHandle>().GetComponent<Collider>().enabled = false;
        }

        fx.GetComponent<ParticleSystem>().Play();
        PoolManager.Pools["Game"].Despawn(fx, 4f);
        if (isPool && !gameObject.name.StartsWith("Main"))
        {
            PoolManager.Pools["Game"].Despawn(transform, 2f, GameMaster.Instance.poolTransform);
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
