﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodFXDelayer : MonoBehaviour
{
	// Max playing blood fx at a moment
	private int playingAllyBloodFx = 0;
	private int maxAllyBloodFx = 6;
	private int playingEnemyBloodFx = 0;
	private int maxEnemyBloodFx = 6;

	private bool allyFxAvailable = true;
	private bool enemyFxAvailable = true;


	public int PlayingEnemyBloodFx { get => playingEnemyBloodFx; }
	public int PlayingAllyBloodFx { get => playingAllyBloodFx; }


	public bool IncreasePlayingAllyBloodFx()
	{
		if (allyFxAvailable && playingAllyBloodFx < maxAllyBloodFx)
		{
			allyFxAvailable = false;
			playingAllyBloodFx++;
			StartCoroutine(StartAllyBloodFxCooldown());
			return true;
		}
		else
		{
			return false;
		}
	}


	private IEnumerator StartAllyBloodFxCooldown()
	{
		yield return new WaitForSeconds(0.2f);
		allyFxAvailable = true;
		yield return new WaitForSeconds(0.6f);
		playingAllyBloodFx--;
	}


	public bool IncreasePlayingEnemyBloodFx()
	{
		if (enemyFxAvailable && playingEnemyBloodFx < maxEnemyBloodFx)
		{
			enemyFxAvailable = false;
			playingEnemyBloodFx++;
			StartCoroutine(StartEnemyBloodFxCooldown());
			return true;
		}
		else
		{
			return false;
		}
	}


	private IEnumerator StartEnemyBloodFxCooldown()
	{
		yield return new WaitForSeconds(0.2f);
		enemyFxAvailable = true;
		yield return new WaitForSeconds(0.6f);
		playingEnemyBloodFx--;
	}
}
