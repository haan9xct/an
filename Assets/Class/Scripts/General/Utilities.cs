﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utilities
{
    public static string ALLY_CHARACTER_LAYER = "Ally Character";

    public static string NEUTRAL_STANDARD_COLLIDER_TAG = "NeutralStandardCollider";
    public static string OBSTACLE_STANDARD_COLLIDER_TAG = "Obstacle Standard Collider";
    public static string FRAGILE_OBSTACLE_STANDARD_COLLIDER = "Fragile Obstacle Standard Collider";
    public static string IDLE_ANIMATION_NAME = "_Locomotion";
    public static string FLIP_ANIMATION_NAME = "_Flip";
    public static string DANCE_ANIMATION_NAME = "_Dance";
    public static string SHOW_OFF_ANIMATION_NAME = "_ShowOff";
    public static string SHOOT1_ANIMATION_NAME = "_Shoot1";

    [Header("Object sizes")]
    public static float RUNNING_PATH_WIDTH = 16f;
    public static float PATH_FENCE_WIDTH = 1f;

    public static float NEUTRAL_ALLY_SIZE = 1f;

    [Header("Movement parameters")]
    public static float CROWDS_STANDARD_MOVING_SPEED = 11f;
    public static float BOOSTING_SPEED_MOVING = 35.2f; // Relative to boosting duration

    [Header("Distance parameters")]

    [Header("Durations")]
    public static float BOOSTING_DURATION = 4.2f;

    [Header("Endgame parameters")]
    public static float ENDGAME_AIMING_TIME = 0.3f;

}

