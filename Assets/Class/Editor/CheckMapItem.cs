﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

[CustomEditor(typeof(LevelController)), CanEditMultipleObjects]
public class CheckMapItem : Editor
{
    public static LevelController groupControl = null;

    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical();
        if (GUILayout.Button("Sync map data"))
        {
            getGroupControl(target);
            doGenEnumEvent();
            AssetDatabase.Refresh();
        }
        GUILayout.EndVertical();

        try
        {
            base.OnInspectorGUI();
        }
        catch (Exception ex)
        {

        }
    }

    private static void getGroupControl(UnityEngine.Object ob)
    {
        if (groupControl == null)
        {
            groupControl = (LevelController)ob;
        }
    }

    private static void doGenEnumEvent()
    {
#if UNITY_EDITOR
        List<string> enumAdjustEventName = new List<string>();
        string levelDirectoryPathDst;
        string levelDirectoryPathSrc;
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            levelDirectoryPathSrc = Application.dataPath + "/Resources/";
            levelDirectoryPathDst = Application.dataPath + "/Prefabs/Modules/";
        }
        else
        {
            levelDirectoryPathSrc = Application.dataPath + "\\Resources\\";
            levelDirectoryPathDst = Application.dataPath + "\\Prefabs\\Modules\\";
        }
        ItemMapModel itemm = Resources.Load<ItemMapModel>(groupControl.prefabStart.pathRes);
        GameObject dstpre = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Modules/Module_Start.prefab", typeof(GameObject)) as GameObject;
        if (copyDataItemMapModel(itemm, dstpre.GetComponent<ItemMapModel>()))
        {
            PrefabUtility.SavePrefabAsset(dstpre);
        }

        for (int i = 0; i < groupControl.mapStart.Count; i++)
        {
            itemm = Resources.Load<ItemMapModel>(groupControl.mapStart[i].pathRes);
            if (groupControl.mapStart[i].gameObject.name.Contains("StartMap_Mixe"))
            {
                dstpre = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Modules/Mixed New Maps/" + groupControl.mapStart[i].gameObject.name + ".prefab", typeof(GameObject)) as GameObject;
            }
            else
            {
                dstpre = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Modules/" + groupControl.mapStart[i].gameObject.name + ".prefab", typeof(GameObject)) as GameObject;
            }
            if (copyDataItemMapModel(itemm, dstpre.GetComponent<ItemMapModel>()))
            {
                PrefabUtility.SavePrefabAsset(dstpre);
            }
        }
        
#endif
    }

    private static bool copyDataItemMapModel(ItemMapModel src, ItemMapModel dst)
    {
        bool re = false;
        if (src != null && dst != null)
        {
            if (dst.type != src.type)
            {
                re = true;
                dst.type = src.type;
            }
            if (dst.length != src.length)
            {
                re = true;
                dst.length = src.length;
            }
            if (dst.isOverBegin != src.isOverBegin)
            {
                re = true;
                dst.isOverBegin = src.isOverBegin;
            }
            if (dst.isEmptyEnd != src.isEmptyEnd)
            {
                re = true;
                dst.isEmptyEnd = src.isEmptyEnd;
            }
            if (dst.isUse4StartLevel != src.isUse4StartLevel)
            {
                re = true;
                dst.isUse4StartLevel = src.isUse4StartLevel;
            }
            if (dst.hardlevel != src.hardlevel)
            {
                re = true;
                dst.hardlevel = src.hardlevel;
            }
            if (dst.posStart != src.posStart)
            {
                re = true;
                dst.posStart = src.posStart;
            }
            if (dst.numAlly != src.numAlly)
            {
                re = true;
                dst.numAlly = src.numAlly;
            }
            if (dst.perAllyGetAble != src.perAllyGetAble)
            {
                re = true;
                dst.perAllyGetAble = src.perAllyGetAble;
            }
            if (dst.perAllyPass != src.perAllyPass)
            {
                re = true;
                dst.perAllyPass = src.perAllyPass;
            }
            if (dst.numEnemy != src.numEnemy)
            {
                re = true;
                dst.numEnemy = src.numEnemy;
            }
           
        }
        return re;
    }

}
